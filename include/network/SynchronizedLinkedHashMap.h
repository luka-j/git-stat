//
// Created by luka on 25.12.21..
//

// TESTS:
// 2-3 tests: void insert(K key, V value) - existing and non-existing value, check if it's in order
// 2 tests: V get(K key) - getting existing and non-existing value
// 2 tests: bool erase(K key) - erasing existing and non-existing value
// ======================
// TOTAL: ~7

#ifndef GITSTAT_SYNCHRONIZEDLINKEDHASHMAP_H
#define GITSTAT_SYNCHRONIZEDLINKEDHASHMAP_H

#include <list>
#include <memory>
#include <QReadWriteLock>
#include <unordered_map>

namespace Network {
template<typename K, typename V>
class SynchronizedLinkedHashMap;
}

template<typename K, typename V>
class Network::SynchronizedLinkedHashMap {
  private:
    std::shared_ptr<std::list<V>> valuesList = std::make_shared<std::list<V>>();
    std::unordered_map<K, typename std::list<V>::iterator> map;
    QReadWriteLock lock;

  public:
    void insert(K key, V value) {
        lock.lockForWrite();
        if (map.contains(key)) {
            auto existingValue = map[key];
            valuesList->erase(existingValue);
            map.erase(key);
        }

        valuesList->push_back(value);
        auto lastElem = std::prev(valuesList->end());
        map.insert(std::make_pair(key, lastElem));
        lock.unlock();
    }

    V get(K key) {
        lock.lockForRead();
        if (!map.contains(key)) {
            lock.unlock();
            return V{};
        }
        auto ret = *(map[key]);
        lock.unlock();
        return ret;
    }

    bool erase(K key) {
        lock.lockForWrite();
        if (!map.contains(key)) {
            lock.unlock();
            return false;
        }
        auto it = map[key];
        valuesList->erase(it);
        map.erase(key);
        lock.unlock();
        return true;
    }

    std::shared_ptr<std::list<V>> getValues() { return this->valuesList; }
};

#endif // GITSTAT_SYNCHRONIZEDLINKEDHASHMAP_H
