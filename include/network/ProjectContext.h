//
// Created by luka on 26.12.21..
//

// TESTS:
// ~3 tests: createOrGet(int groupId) - consecutive, with softErase, with erase
// 2 tests: size - after erase / softErase
// 2 tests: isEmpty - after erase / softErase
// 2 tests: exists - after erase / softErase
// 2 test: insertCommits / getCommit - getCommit non-existing; insert, than get
// 1 test: get - non-existing
// 2 tests: addCommitCount - before / after finishCommitCount
// 2-3 tests: incrementCommitDownloaded / areAllCommitsDownloaded - increment until finished,
//            check areAllCommitsDownloaded beforehand, check areAllCommitsDownloaded on zero
// ======================
// TOTAL: 17

#ifndef GITSTAT_PROJECTCONTEXT_H
#define GITSTAT_PROJECTCONTEXT_H

#include "GitLabCommitResponse.h"
#include "SynchronizedLinkedHashMap.h"
#include <QMutex>
#include <unordered_map>

namespace Network {
class ProjectContext;
}

class Network::ProjectContext {
    class ProjectContextData {
      public:
        int projectId;
        long long totalCommits      = 0;
        long long downloadedCommits = 0;
        bool finishedCommitCount    = false;
        bool maybeDangling          = false;
        Network::SynchronizedLinkedHashMap<std::string, std::shared_ptr<GitLabCommitResponse>> commits;
    };

  public:
    bool exists(int projectId);
    bool incrementCommitDownloaded(int projectId);
    bool areAllCommitsDownloaded(int projectId);
    bool erase(int projectId);
    bool softErase(int projectId);
    std::shared_ptr<ProjectContextData> createOrGet(int projectId);
    bool isEmpty();
    std::shared_ptr<ProjectContextData> get(int projectId);
    std::shared_ptr<GitLabCommitResponse> getCommit(int projectId, std::string commitId);
    std::shared_ptr<std::list<std::shared_ptr<GitLabCommitResponse>>> getCommits(int projectId);
    bool addCommitCount(int projectId, int commitCount);
    bool finishCommitCount(int projectId);
    bool insertCommits(int projectId, const std::shared_ptr<GitLabCommitResponse>& commit);

    unsigned long size();
    long long getDownloadedCommitsCount(int projectId);
    long long getTotalCommitsCount(int projectId);
    bool isFinishedCommitCount(int projectId);

  private:
    std::unordered_map<int, std::shared_ptr<ProjectContextData>> contextMap;
    QMutex insertMutex;
};

#endif // GITSTAT_PROJECTCONTEXT_H
