//
// Created by luka on 26.12.21..
//

#ifndef GITSTAT_NETWORKREQUESTER_H
#define GITSTAT_NETWORKREQUESTER_H

#include "NetworkContext.h"
#include "NetworkResponseParser.h"
#include "NetworkUtils.h"
#include <memory>
#include <QObject>
#include <QVariant>
#include <utility>

namespace Network {
class NetworkRequester;
class NetworkResponseParser;
class GitLabResponseHandler;
}

class Network::NetworkRequester : public QObject {
    Q_OBJECT

  private:
    std::shared_ptr<NetworkContext> networkContext = std::make_shared<NetworkContext>();
    QNetworkAccessManager* qNam;
    NetworkResponseParser* responseParser;

  public:
    NetworkRequester(QObject* parent,
                     std::shared_ptr<NetworkContext> networkContext,
                     QNetworkAccessManager* qNam,
                     NetworkResponseParser* responseParser);

    virtual void doGetRequest(const std::string& urlString,
                              const ResponseHandlerFunc& responseHandlerFunc,
                              int requestId,
                              const std::string& commitId = "");

  signals:
    void fireError(std::string error, int requestId);
};

#endif // GITSTAT_NETWORKREQUESTER_H
