//
// Created by luka on 22.11.21..
//

// TESTS:
// ~2 testa: GitLabGroupResponse(QJsonObject& json) - Parsiranje JSONa u objekat

#ifndef GITSTAT_GITLABGROUPRESPONSE_H
#define GITSTAT_GITLABGROUPRESPONSE_H

#include "GitLabProjectResponse.h"
#include <ostream>
#include <QJsonArray>
#include <QJsonValue>
#include <string>
#include <vector>

namespace Network {
class GitLabGroupResponse;
}

class Network::GitLabGroupResponse {
  public:
    int id;
    std::string webUrl;
    std::string name;
    std::string path;
    std::string description;
    std::string avatarUrl;
    std::string fullName;
    std::string fullPath;
    std::string createdAt;
    int parentId;
    std::vector<std::shared_ptr<GitLabProjectResponse>> projects;

    explicit GitLabGroupResponse(QJsonObject& json);

    operator QString() const; // NOLINT(google-explicit-constructor)
};

#endif // GITSTAT_GITLABGROUPRESPONSE_H
