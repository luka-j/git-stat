//
// Created by luka on 26.12.21..
//

// TESTS:
// ~3 tests: createOrGet(int groupId) - consecutive, with softErase, with erase
// 2 tests: erase / softErase - check size
// 3 tests: setData / get - get non-existing, get after set, get after softErase
// 1 test: insertProject - insert, than get
// ======================
// TOTAL: ~9

#ifndef GITSTAT_GROUPCONTEXT_H
#define GITSTAT_GROUPCONTEXT_H

#include "GitLabGroupResponse.h"
#include <memory>
#include <QMutex>
#include <unordered_map>

namespace Network {
class GroupContext;
}

class Network::GroupContext {

    class GroupContextData {
      public:
        std::shared_ptr<Network::GitLabGroupResponse> groupInfo;
        bool maybeDangling = false;
    };

  public:
    bool erase(int groupId);
    bool softErase(int groupId);
    std::shared_ptr<GroupContextData> createOrGet(int groupId);
    bool setData(const std::shared_ptr<Network::GitLabGroupResponse>& data);
    bool insertProject(int groupId, const std::shared_ptr<GitLabProjectResponse>& project);
    std::shared_ptr<GitLabGroupResponse> get(int groupId);
    unsigned long size();

  private:
    std::unordered_map<int, std::shared_ptr<GroupContextData>> contextMap;
    QMutex insertMutex;
};

#endif // GITSTAT_GROUPCONTEXT_H
