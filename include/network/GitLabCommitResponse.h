//
// Created by luka on 22.11.21..
//

// TESTS:
// ~2 testa: GitLabCommitResponse(QJsonObject& json) - Parsiranje JSONa u objekat
// ~2 testa: FileChange::FileChange(QJsonObject& json) - Parsiranje JSONa u objekat
// ====================
// TOTAL: 4

#ifndef GITSTAT_GITLABCOMMITRESPONSE_H
#define GITSTAT_GITLABCOMMITRESPONSE_H

#include "NetworkUtils.h"
#include <ostream>
#include <QJsonArray>
#include <QJsonObject>
#include <string>
#include <utility>
#include <vector>

namespace Network {
class GitLabCommitResponse;
}

class Network::GitLabCommitResponse {
  public:
    struct CommitStats {
        int additions;
        int deletions;
        int total;

        operator QString() const; // NOLINT(google-explicit-constructor)
        operator std::string() const; // NOLINT(google-explicit-constructor)
    };
    struct FileChange {
        std::string oldFilename;
        std::string newFilename;
        bool isFileCreated;
        bool isFileRenamed;
        bool isFileDeleted;

        explicit FileChange(const QJsonObject& json);

        operator QString() const; // NOLINT(google-explicit-constructor)
        operator std::string() const; // NOLINT(google-explicit-constructor)
    };

    std::string id;
    std::string shortId;
    std::string createdAt;
    std::vector<std::string> parentIds;
    std::string title;
    std::string message;
    std::string authorName;
    std::string authorEmail;
    std::string authoredDate;
    std::string committerName;
    std::string committerEmail;
    std::string committedDate;
    std::string webUrl;
    CommitStats stats{};
    std::vector<std::shared_ptr<FileChange>> fileChanges;

    explicit GitLabCommitResponse(QJsonObject& json);

    operator QString() const;
};

#endif // GITSTAT_GITLABCOMMITRESPONSE_H
