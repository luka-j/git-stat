//
// Created by luka on 22.11.21..
//

// TESTS:
// 1 test: getProjectInfo(int id) - check that FakeNetworkRequester is called
// 2 tests: getAllProjectsInGroup(int id) - with and without existing context
// 2 tests: getCommitsForProject(int id)  - with and without existing context
// ======================
// TOTAL: 5 tests

#ifndef GITSTAT_GITLABNETWORKING_H
#define GITSTAT_GITLABNETWORKING_H

#include "GitLabResponseHandler.h"
#include "GroupContext.h"
#include "include/network/GitLabCommitResponse.h"
#include "include/network/GitLabGroupResponse.h"
#include "include/network/GitLabProjectResponse.h"
#include "NetworkContext.h"
#include "NetworkRequester.h"
#include "NetworkResponseParser.h"
#include "ProjectContext.h"
#include "SynchronizedLinkedHashMap.h"
#include <exception>
#include <QDir>
#include <QFile>
#include <QMutex>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QObject>
#include <QReadWriteLock>
#include <QThread>
#include <unordered_map>
#include <unordered_set>
#include <vector>

namespace Network {
class GitLabNetworking;
}

class Network::GitLabNetworking : public QObject {
    Q_OBJECT

  private:
    static GitLabNetworking* instance;
    explicit GitLabNetworking(QObject* parent = nullptr);
    QThread* executionThread;
    void init();

  public:
    static GitLabNetworking* getInstance();

    GitLabNetworking(GitLabNetworking& other) = delete;
    void operator=(const GitLabNetworking&) = delete;

    // for tests only
    GitLabNetworking(QObject* parent,
                     QThread* executionThread,
                     QNetworkAccessManager* qNam,
                     NetworkResponseParser* responseParser,
                     NetworkRequester* networkRequester,
                     GitLabResponseHandler* responseHandler,
                     std::shared_ptr<ProjectContext> projectContext,
                     std::shared_ptr<GroupContext> groupContext,
                     std::shared_ptr<NetworkContext> networkContext);

  private slots:
    void handleCheckAuthTokenRequest();

    void getProjectInfoImpl(int id);
    void getAllProjectsInGroupImpl(int id);
    void getCommitsForProjectImpl(int id);
    void setAuthTokenImpl(const std::string& token);

    void fireError(std::string message, int requestId);

  signals:
    void getProjectInfo(int id);
    void getAllProjectsInGroup(int id);
    void getCommitsForProject(int id);
    void setAuthToken(const std::string& token);

    void downloadedProjectInfo(std::shared_ptr<GitLabProjectResponse> projectInfo);
    void downloadedProjectsInGroup(std::shared_ptr<GitLabGroupResponse> groupInfo);
    void downloadedCommitInfo(int projectId, std::shared_ptr<std::list<std::shared_ptr<GitLabCommitResponse>>>);
    void handleNetworkError(std::string message);

    void authCompleted(bool success, std::string text);
    void rateLimited(int timeoutSeconds);
    void allRequestsFinished();

  private:
    QNetworkAccessManager* qNam;

    NetworkResponseParser* responseParser;
    NetworkRequester* networkRequester;
    GitLabResponseHandler* responseHandler;

    std::shared_ptr<ProjectContext> projectContext;
    std::shared_ptr<GroupContext> groupContext;
    std::shared_ptr<NetworkContext> networkContext;
};

#endif // GITSTAT_GITLABNETWORKING_H
