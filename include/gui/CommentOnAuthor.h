#ifndef COMMENT_ON_AUTHOR_H
#define COMMENT_ON_AUTHOR_H

#include <QDialog>

namespace Ui {
class CommentOnAuthor;
}

class CommentOnAuthor : public QDialog {
    Q_OBJECT

  public:
    explicit CommentOnAuthor(QWidget* parent = nullptr);
    ~CommentOnAuthor() override;

    Ui::CommentOnAuthor* getUi();

  private:
    Ui::CommentOnAuthor* ui;
};

#endif // COMMENT_ON_AUTHOR_H
