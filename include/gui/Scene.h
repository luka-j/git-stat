#ifndef SCENE_H
#define SCENE_H

#include "AuthorNode.h"
#include "CommitNode.h"
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>

class Scene : public QGraphicsScene {
    Q_OBJECT

  public:
    explicit Scene(QObject* parent = nullptr);

    void mousePressEvent(QGraphicsSceneMouseEvent* event) override;

  signals:
    void author_node_clicked(Author author);
    void commit_node_clicked(QVector<QSharedPointer<Commit>>* commits);
};

#endif // SCENE_H
