#ifndef AUTHORNODE_H
#define AUTHORNODE_H

#define NODE_HEIGHT 100
#define NODE_WIDTH 300

#include "include/data/Author.h"
#include "include/data/Commit.h"
#include <iostream>
#include <QGraphicsItem>
#include <QString>

class AuthorNode : public QGraphicsItem {

  public:
    AuthorNode();

    explicit AuthorNode(Author a);

    [[nodiscard]] QRectF boundingRect() const override;
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = nullptr) override;

    [[nodiscard]] static qint32 Height() ;
    [[nodiscard]] static qint32 Width() ;

    Author getAuthor();

  private:
    Author author;
};

#endif // AUTHORNODE_H
