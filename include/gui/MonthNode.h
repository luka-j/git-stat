#ifndef MONTHNODE_H
#define MONTHNODE_H

#include <QGraphicsItem>
#include <QString>

class MonthNode : public QGraphicsItem {

  public:
    MonthNode();

    MonthNode(QString name);

    [[nodiscard]] QRectF boundingRect() const override;
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = nullptr) override;

    QString getName();
    [[nodiscard]] static qint32 Height() ;
    [[nodiscard]] static qint32 Width() ;

  private:
    QString name;
};

#endif // MONTHNODE_H
