#ifndef COMMITNODE_H
#define COMMITNODE_H

#define NODE_DIAMETER 13

#include "include/data/Commit.h"
#include <QBrush>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QPainter>
#include <QSharedPointer>

class CommitNode : public QGraphicsItem {
  public:
    explicit CommitNode(QVector<QSharedPointer<Commit>>* commits);

    [[nodiscard]] QRectF boundingRect() const override;
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = nullptr) override;

    void setBrush(QColor color);
    QBrush getBrush();
    QVector<QSharedPointer<Commit>>* getCommits();
    [[nodiscard]] static qint32 Diameter() ;

  private:
    QVector<QSharedPointer<Commit>>* commits;
    QBrush brush;
};

#endif // COMMITNODE_H
