#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "include/gui/SingleRepoPage.h"
#include "Scene.h"
#include <QMainWindow>
#include <QSharedPointer>
#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
    Q_OBJECT

  public:
    explicit MainWindow(QWidget* parent = nullptr);
    ~MainWindow() override;

    Ui::MainWindow* getUi();
    Scene* getScene();
    QSharedPointer<SingleRepoData> getData();

    static void show_content_of_the_second_page(PlotTimeline* plot_timeline,
                                         CheckBoxList* check_box,
                                         SingleRepoPage* page_two);

  public slots:
    static void on_pbToken_clicked();
    static void on_pbAdd_clicked();
    void seeMoreDetails(const QSharedPointer<Repo>& repo);

  private:
    Ui::MainWindow* ui;
    Scene* scene;
};
#endif // MAINWINDOW_H
