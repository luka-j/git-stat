#ifndef COMMENT_ON_REPO_H
#define COMMENT_ON_REPO_H

#include <QDialog>

namespace Ui {
class CommentOnRepo;
}

class CommentOnRepo : public QDialog {
    Q_OBJECT

  public:
    explicit CommentOnRepo(QWidget* parent = nullptr);
    ~CommentOnRepo() override;

    Ui::CommentOnRepo* getUi();

  private:
    Ui::CommentOnRepo* ui;
};

#endif // COMMENT_ON_REPO_H
