#ifndef REPONODE_H
#define REPONODE_H

#include "include/data/DataProcessing.h"
#include "include/data/Repo.h"
#include "include/gui/MainWindow.h"

#include <memory>
#include <QSharedPointer>
#include <QWidget>
#include <string>

namespace Ui {
class RepoNode;
}

class RepoNode : public QWidget {
    Q_OBJECT

  public:
    explicit RepoNode(QWidget* parent = nullptr);
    ~RepoNode() override;

    [[nodiscard]] Ui::RepoNode* getUi() const;
    [[nodiscard]] int getRow() const;
    [[nodiscard]] int getColumn() const;
    [[nodiscard]] QSharedPointer<Repo> getRepo() const;

    void setUrlAsLink(const QString& url);
    void setRow(int newRow);
    void setColumn(int newColumn);
    void setRepo(QSharedPointer<Repo> newRepo);
    void setWindow(MainWindow* w);
    void setNode();

  public slots:
    void pbRefreshRepositoryClicked();
    void pbSeeMoreDetailsClicked();
    void pbRemoveClicked();

  signals:
    void seeMoreDetails(QSharedPointer<Repo> repo);
    void removeRepository(RepoNode* r);

  private:
    Ui::RepoNode* ui;
    DataProcessing* dp;
    QSharedPointer<Repo> repo;
    MainWindow* w;

    int row{};
    int column{};
};

#endif // REPONODE_H
