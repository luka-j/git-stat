#ifndef MYBUTTON_H
#define MYBUTTON_H

#include <QDebug>
#include <QPushButton>

class MyButton : public QPushButton {
    Q_OBJECT
  public:
    explicit MyButton(const QString& text);

  signals:
    void my_button_clicked(int i);

  private slots:
    void reemitClicked();
};

#endif // MYBUTTON_H
