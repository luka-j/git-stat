#ifndef POPUPTOKENDIALOG_H
#define POPUPTOKENDIALOG_H

#include "include/network/GitLabNetworking.h"
#include <QDialog>

namespace Ui {
class PopupTokenDialog;
}

class PopupTokenDialog : public QDialog {
    Q_OBJECT

  public:
    explicit PopupTokenDialog(QWidget* parent = nullptr);
    ~PopupTokenDialog() override;

  public slots:
    void AddToken();

  private:
    Ui::PopupTokenDialog* ui;
};

#endif // POPUPTOKENDIALOG_H
