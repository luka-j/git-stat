#ifndef PAGE_TWO_H
#define PAGE_TWO_H

#include "include/data/Author.h"
#include "include/data/Comment.h"
#include "include/gui/CheckBoxList.h"
#include "include/gui/CommentOnAuthor.h"
#include "include/gui/CommentOnCommit.h"
#include "include/gui/CommentOnRepo.h"
#include "include/gui/MyButton.h"
#include "include/gui/PlotTimeline.h"
#include "include/gui/Scene.h"
#include "include/gui/SingleRepoData.h"
#include <QDateTime>
#include <QFormLayout>
#include <QLineF>
#include <QMessageBox>
#include <QObject>

namespace Ui {
class MainWindow;
}

class SingleRepoPage : public QObject {
    Q_OBJECT

  private:
    static SingleRepoPage* instance;
    explicit SingleRepoPage();

  public:
    static SingleRepoPage* getInstance();

    void create_repo_info_box();
    void select_duration_of_the_timeline();
    void connections();
    void connect_once();

    void setUi(Ui::MainWindow* ui);
    void setScene(Scene* scene);
    void setData(QSharedPointer<SingleRepoData> data);
    void setCheckBox(CheckBoxList* cb_l);
    void setPlotTimeline(PlotTimeline* pt);
    void draw_on_a_graphics_view();

  signals:
    void chosen_timeline(int index);
    void comment_created(QSharedPointer<Comment> comment, QString& idAuthor);
    void create_report(int repoId);

  public slots:
    void cb_chose_timeline_activated();
    void send_an_email();
    void pb_add_comment_for_repo_clicked();
    void pb_repo_comment_clicked();
    void pb_add_comment_for_commit_clicked(int i);
    void author_node_clicked(const Author& author);
    void pb_author_comment_clicked();
    void pb_commit_comment_clicked();
    void pb_back_to_projects_clicked();
    void commit_node_clicked(QVector<QSharedPointer<Commit>>* commits);
    void create_report_clicked();

    static void delete_layout_content(QFormLayout* layout);

  protected:
    Ui::MainWindow* ui = nullptr;
    Scene* scene       = nullptr;

    CommentOnRepo* comment_repo     = nullptr;
    CommentOnAuthor* comment_author = nullptr;
    CommentOnCommit* comment_commit = nullptr;

    Author author;
    QVector<QSharedPointer<Commit>>* commits = nullptr;
    int index_of_commit                      = 0;

    QSharedPointer<SingleRepoData> data;
    CheckBoxList* check_box     = nullptr;
    PlotTimeline* plot_timeline = nullptr;
};
#endif // PAGE_TWO_H
