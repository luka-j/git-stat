#ifndef POPUPADDDIALOG_H
#define POPUPADDDIALOG_H

#include "include/data/DataProcessing.h"
#include <QDialog>

namespace Ui {
class PopupAddDialog;
}

class PopupAddDialog : public QDialog {
    Q_OBJECT

  public:
    explicit PopupAddDialog(QWidget* parent = nullptr);
    ~PopupAddDialog() override;

  public slots:
    void AddNewRepositoryOrGroup();
    static void showPopupTokenDialog();

  private:
    Ui::PopupAddDialog* ui;
    DataProcessing* dp;
};

#endif // POPUPADDDIALOG_H
