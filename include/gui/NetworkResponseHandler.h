#ifndef NETWORKRESPONSEHANDLER_H
#define NETWORKRESPONSEHANDLER_H

#include "include/gui/MainWindow.h"
#include "include/network/GitLabNetworking.h"

namespace Ui {
class MainWindow;
}
namespace Network {
class GitLabNetworking;
}

class NetworkResponseHandler : public QObject {
    Q_OBJECT

  public:
    explicit NetworkResponseHandler(Ui::MainWindow* ui);
    void setMessage(const QString& message);

  public slots:
    static void authorization(bool valid, const std::string& text);
    static void downloadError(const std::string& error);
    void allRequestsFinished();
    void rateLimited(int timeoutSeconds);
    void clearMessage();
    void countDown();

  private:
    Ui::MainWindow* ui;
    QSharedPointer<QTimer> qt;
    int tmt = 0;
};

#endif // NETWORKRESPONSEHANDLER_H
