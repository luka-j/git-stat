//
// Created by caca on 27.11.21..
//

#ifndef GITSTAT_DATAPROCESSING_H
#define GITSTAT_DATAPROCESSING_H

#include "include/data/ProcessingComments.h"
#include "include/data/ProcessingCommit.h"
#include "include/data/ProcessingRepo.h"
#include "include/gui/SingleRepoPage.h"
#include "include/network/GitLabNetworking.h"

#include <filesystem>
#include <QDir>
#include <QJsonArray>
#include <QJsonValue>
#include <QSharedPointer>
#include <vector>

// class Repo;
class SingleRepoPage;

class DataProcessing : public QObject {
    Q_OBJECT

  private:
    static DataProcessing* instance;
    explicit DataProcessing(QObject* parent = nullptr);

  public:
    static DataProcessing* getInstance();
    static const inline QString FILE_PATH = QDir::homePath().append("/").append("Git-stat/").append("dataInfo");
    static inline QString repoPath        = FILE_PATH + "/repos.json";
    static inline QString reportPath      = QDir::homePath().append("/").append("Git-stat/");

    void getGroupInfo(int groupId);
    void getProjectInfo(int repoId, bool needToRefresh = false);
    void removeRepo(int id);
    [[nodiscard]] bool checkIfOnDrive(int id) const;
    void setFileNameForTests(QString& newPath);
    static void setFileNameForReportTest(QString& newPath);
    void setReposForReportTest(QSharedPointer<QVector<QSharedPointer<Repo>>> testRepo);

    QSharedPointer<QVector<QSharedPointer<Repo>>> onDisk =
      QSharedPointer<QVector<QSharedPointer<Repo>>>::create(QVector<QSharedPointer<Repo>>());

  public slots:
    void saveProjectInfo(const std::shared_ptr<Network::GitLabProjectResponse>& projectInfo);
    void saveProjectsInGroup(const std::shared_ptr<Network::GitLabGroupResponse>& groupInfo);
    void saveCommitInfo(int projectId, const std::shared_ptr<std::list<std::shared_ptr<Network::GitLabCommitResponse>>>&);

    void addComment(const QSharedPointer<Comment>& comment, QString email);
    void createReport(int repoId) const;

  signals:
    void readyProjectInfo(QSharedPointer<Repo> repo);
    void readyWithCommits(QSharedPointer<Repo> repo);

  private:
    QDir dataDir;
    Network::GitLabNetworking* network;
    ProcessingRepo* repoProcessor;
    ProcessingComments* commentProcessor;
    ProcessingCommit* commitProcessor;
    SingleRepoPage* page_two;
};

#endif // GITSTAT_DATAPROCESSING_H
