#ifndef COMMIT_H
#define COMMIT_H

#include "Author.h"
#include "include/network/GitLabCommitResponse.h"
#include <iostream>
#include <string>
#include <vector>

class Commit {

  public:
    struct CommitStats {
        bool operator==(const CommitStats& rhs) const;

        bool operator!=(const CommitStats& rhs) const;

        int additions;
        int deletions;
        int total;
    };
    struct FileChange {
        QString oldFilename;
        QString newFilename;
        bool isFileCreated;
        bool isFileRenamed;
        bool isFileDeleted;

        explicit FileChange(Network::GitLabCommitResponse::FileChange& fc);
        FileChange(QString old, QString newf, bool creat, bool rename, bool deleted);

        bool operator==(const FileChange& rhs) const;

        bool operator!=(const FileChange& rhs) const;
    };

    Commit(QString id,
           QDateTime createdAt,
           QString title,
           QString message,
           Author author,
           QString webUrl,
           const CommitStats& stats,
           QVector<FileChange> changes,
           QVector<QSharedPointer<Comment>> comm = {});

    ~Commit();
    Commit& operator=(const Commit& commit);
    Commit(Commit& commit);

    [[nodiscard]] const QString& getId() const;
    [[nodiscard]] const QDateTime& getCreatedAt() const;
    [[nodiscard]] const QString& getTitle() const;
    [[nodiscard]] const QString& getMessage() const;
    [[nodiscard]] const Author& getAuthor() const;
    [[nodiscard]] const QString& getWebUrl() const;
    [[nodiscard]] const CommitStats& getStats() const;
    [[nodiscard]] const QVector<FileChange>& getFileChanges() const;
    [[nodiscard]] QVector<QSharedPointer<Comment>> getComments() const;
    void addComment(const QSharedPointer<Comment>& comm);
    void setComment(QVector<QSharedPointer<Comment>> comm);

    bool operator==(const Commit& rhs) const;

    bool operator!=(const Commit& rhs) const;

  private:
    QString id;
    Author author; // name,email
    QDateTime createdAt;
    QString title;
    QString message;
    QString webUrl;
    CommitStats stats;
    QVector<FileChange> changes;
    QVector<QSharedPointer<Comment>> comments;
};

inline bool operator<(const Commit& a, const Commit& b) {
    return a.getCreatedAt() < b.getCreatedAt();
}

#endif // COMMIT_H
