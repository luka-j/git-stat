#ifndef COMMENT_H
#define COMMENT_H

#include <iostream>
#include <QDate>
#include <QString>

enum class TypeOfComm { author, commit, repo };

class Comment {

  public:
    Comment(const TypeOfComm& type, QString parentId, QDateTime createdAt, QString message, int repoId);

    [[nodiscard]] TypeOfComm getTypeOfComm() const;
    static TypeOfComm getTypeOfComm(const QString& typeCom);
    [[nodiscard]] QString getParentId() const;
    [[nodiscard]] QDateTime getCreatedAt() const;
    [[nodiscard]] QString getMessage() const;
    [[nodiscard]] QString typeToString() const;
    [[nodiscard]] int getRepoId() const;

    bool operator<(const Comment& comm) const;
    bool operator==(const Comment& comm) const;

  private:
    TypeOfComm typeComment;
    QString parentId; // author-email/repoId/groupId
    QDateTime createdAt;
    QString message;
    int repoId;
};

#endif // COMMENT_H
