FROM archlinux as builder

RUN pacman-key --init && pacman --noconfirm -Sy base-devel git cmake clang qt6-base

RUN mkdir /workspace
COPY CMakeLists.txt /workspace/
COPY src /workspace/src
COPY include /workspace/include
COPY tests /workspace/tests
WORKDIR /workspace

RUN cmake -G "Unix Makefiles" .
RUN make GitStat


FROM archlinux

RUN pacman-key --init && pacman --noconfirm -Sy qt6-base x11vnc xorg-server-xvfb
ENV DISPLAY=:1

RUN mkdir /app
WORKDIR /app
COPY --from=builder /workspace/GitStat /app/GitStat
RUN echo -e '#!/bin/bash\nXvfb $DISPLAY -screen 0 1024x768x16 &\nx11vnc -display $DISPLAY -bg -forever -nopw -quiet -listen localhost -xkb && /app/GitStat' > /app/start.sh
RUN chmod +x /app/start.sh
ENTRYPOINT /app/start.sh