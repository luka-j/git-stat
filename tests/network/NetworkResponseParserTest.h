//
// Created by luka on 28.12.21..
//

#ifndef GITSTAT_NETWORKRESPONSEPARSERTEST_H
#define GITSTAT_NETWORKRESPONSEPARSERTEST_H

#include "include/network/NetworkResponseParser.h"
#include <catch2/catch_test_macros.hpp>

TEST_CASE("NetworkResponseParser::retryAfter", "[method]") {
    SECTION("When calling method with null reply, fire error and don't retry anything") {
        //Arrange
        std::shared_ptr<NetworkContext> networkContext = std::make_shared<NetworkContext>();
        FakeNetworkRequester requester(networkContext);
        NetworkResponseParser responseParser(nullptr, networkContext);
        responseParser.setNetworkRequester(&requester);
        QSignalSpy signalSpy(&responseParser, &NetworkResponseParser::fireError);

        //Act
        responseParser.retryAfter(nullptr, 5, false);

        //Assert
        REQUIRE(signalSpy.count() == 1);
        auto requestIdFromSignal = signalSpy.at(0).value(1).value<int>();
        REQUIRE(requestIdFromSignal == 0);
        REQUIRE(requester.timesInvoked() == 0);
        REQUIRE(networkContext->lastSleepTime == 0);
    }

    SECTION("When calling method without response handler, don't do anything") {
        //Arrange
        std::shared_ptr<NetworkContext> networkContext = std::make_shared<NetworkContext>();
        FakeNetworkRequester requester(networkContext);
        NetworkResponseParser responseParser(nullptr, networkContext);
        responseParser.setNetworkRequester(&requester);
        FakeQNetworkReply networkReply;
        QSignalSpy signalSpy(&responseParser, &NetworkResponseParser::fireError);

        //Act
        responseParser.retryAfter(&networkReply, 5, false);

        //Assert
        REQUIRE(signalSpy.count() == 0);
        REQUIRE(requester.timesInvoked() == 0);
        REQUIRE(networkContext->lastSleepTime == 0);
    }

    SECTION("When calling method with forceSleep, sleep even if request is new and haven't slept soon") {
        //Arrange
        const int requestId = 2;

        std::shared_ptr<NetworkContext> networkContext = std::make_shared<NetworkContext>();
        FakeNetworkRequester requester(networkContext);
        NetworkResponseParser responseParser(nullptr, networkContext);
        responseParser.setNetworkRequester(&requester);
        FakeQNetworkReply networkReply;
        QSignalSpy signalSpy(&responseParser, &NetworkResponseParser::fireError);
        networkReply.setProperty(PROP_HANDLER, 1);
        networkReply.setProperty(PROP_TIME_CREATED, (long long)getUnixTimestamp());
        networkReply.setProperty(PROP_REQUEST_ID, requestId);

        //Act
        responseParser.retryAfter(&networkReply, 0, true);

        //Assert
        REQUIRE(signalSpy.count() == 0);
        REQUIRE(networkContext->lastSleepTime > 0);
        REQUIRE(requester.timesInvoked() == 1);
        REQUIRE(requester.getLastRequestId() == requestId);
    }

    SECTION("When calling method with stale request, don't sleep before retrying it") {
        //Arrange
        const int requestId = 2;

        std::shared_ptr<NetworkContext> networkContext = std::make_shared<NetworkContext>();
        FakeNetworkRequester requester(networkContext);
        NetworkResponseParser responseParser(nullptr, networkContext, 0, 1);
        responseParser.setNetworkRequester(&requester);
        FakeQNetworkReply networkReply;
        QSignalSpy signalSpy(&responseParser, &NetworkResponseParser::fireError);
        networkReply.setProperty(PROP_HANDLER, 1);
        networkReply.setProperty(PROP_TIME_CREATED, (long long)getUnixTimestamp()-10);
        networkReply.setProperty(PROP_REQUEST_ID, requestId);
        int lastSleepTime = getUnixTimestamp();
        networkContext->lastSleepTime = lastSleepTime;

        //Act
        responseParser.retryAfter(&networkReply, 5, false);

        //Assert
        REQUIRE(signalSpy.count() == 0);
        REQUIRE(networkContext->lastSleepTime == lastSleepTime);
        REQUIRE(requester.timesInvoked() == 1);
        REQUIRE(requester.getLastRequestId() == requestId);
    }

    SECTION("When calling method and we've slept recently, don't sleep again before retrying") {
        //Arrange
        const int requestId = 2;

        std::shared_ptr<NetworkContext> networkContext = std::make_shared<NetworkContext>();
        FakeNetworkRequester requester(networkContext);
        NetworkResponseParser responseParser(nullptr, networkContext, 10, 100);
        responseParser.setNetworkRequester(&requester);
        FakeQNetworkReply networkReply;
        QSignalSpy signalSpy(&responseParser, &NetworkResponseParser::fireError);
        networkReply.setProperty(PROP_HANDLER, 1);
        networkReply.setProperty(PROP_TIME_CREATED, (long long)getUnixTimestamp());
        networkReply.setProperty(PROP_REQUEST_ID, requestId);
        int lastSleepTime = getUnixTimestamp();
        networkContext->lastSleepTime = lastSleepTime;

        //Act
        responseParser.retryAfter(&networkReply, 5, false);

        //Assert
        REQUIRE(signalSpy.count() == 0);
        REQUIRE(networkContext->lastSleepTime == lastSleepTime);
        REQUIRE(requester.timesInvoked() == 1);
        REQUIRE(requester.getLastRequestId() == requestId);
    }

    SECTION("When calling method with recent reply and we haven't slept recently, sleep before retrying") {
        //Arrange
        const int requestId = 2;

        std::shared_ptr<NetworkContext> networkContext = std::make_shared<NetworkContext>();
        FakeNetworkRequester requester(networkContext);
        NetworkResponseParser responseParser(nullptr, networkContext, 10, 10);
        responseParser.setNetworkRequester(&requester);
        FakeQNetworkReply networkReply;
        QSignalSpy signalSpy(&responseParser, &NetworkResponseParser::fireError);
        networkReply.setProperty(PROP_HANDLER, 1);
        networkReply.setProperty(PROP_TIME_CREATED, (long long)getUnixTimestamp());
        networkReply.setProperty(PROP_REQUEST_ID, requestId);

        //Act
        responseParser.retryAfter(&networkReply, 0, false);

        //Assert
        REQUIRE(signalSpy.count() == 0);
        REQUIRE(networkContext->lastSleepTime > 0);
        REQUIRE(requester.timesInvoked() == 1);
        REQUIRE(requester.getLastRequestId() == requestId);
    }
}

TEST_CASE("NetworkResponseParser::parseNetworkError", "[method]") {
    SECTION("When method is called with non-error reply, do nothing") {
        //Arrange
        std::shared_ptr<NetworkContext> networkContext = std::make_shared<NetworkContext>();
        FakeNetworkRequester requester(networkContext);
        NetworkResponseParser responseParser(nullptr, networkContext, 10, 10);
        responseParser.setNetworkRequester(&requester);
        FakeQNetworkReply networkReply;
        QSignalSpy signalSpy(&responseParser, &NetworkResponseParser::fireError);

        //Act
        responseParser.parseNetworkError(&networkReply);

        //Assert
        REQUIRE(networkContext->lastSleepTime == 0);
        REQUIRE(signalSpy.count() == 0);
        REQUIRE(requester.timesInvoked() == 0);
    }

    SECTION("When method is called with rate limited error and Retry-After header, retry request after period specified in header") {
        //Arrange
        const int requestId = 1;

        std::shared_ptr<NetworkContext> networkContext = std::make_shared<NetworkContext>();
        FakeNetworkRequester requester(networkContext);
        NetworkResponseParser responseParser(nullptr, networkContext, 10, 10);
        responseParser.setNetworkRequester(&requester);
        FakeQNetworkReply networkReply;
        networkReply.setProperty(PROP_REQUEST_ID, requestId);
        networkReply.setProperty(PROP_HANDLER, 1);
        networkReply.setProperty(PROP_TIME_CREATED, QVariant::fromValue(getUnixTimestamp()));
        networkReply.setHttpStatusCode(429, "Too many requests");
        networkReply.setError(QNetworkReply::UnknownContentError, "429");
        networkReply.setRawHeader("Retry-After", "-1");
        QSignalSpy rateLimitedSignalSpy(&responseParser, &NetworkResponseParser::rateLimited);
        QSignalSpy errorSignalSpy(&responseParser, &NetworkResponseParser::fireError);

        //Act
        responseParser.parseNetworkError(&networkReply);

        //Assert
        REQUIRE(networkContext->lastSleepTime > 0);
        REQUIRE(errorSignalSpy.count() == 0);
        REQUIRE(rateLimitedSignalSpy.count() == 2);
        REQUIRE(rateLimitedSignalSpy.at(0).value(0).value<int>() == 0);
        REQUIRE(requester.timesInvoked() == 1);
        REQUIRE(requester.getLastRequestId() == requestId);
    }

    SECTION("When method is called with rate limited error and no Retry-After header, retry request after hardRateLimitCoolOffTime") {
        //Arrange
        const int requestId = 1;

        std::shared_ptr<NetworkContext> networkContext = std::make_shared<NetworkContext>();
        FakeNetworkRequester requester(networkContext);
        NetworkResponseParser responseParser(nullptr, networkContext, 10, 10, 10, 0);
        responseParser.setNetworkRequester(&requester);
        FakeQNetworkReply networkReply;
        networkReply.setProperty(PROP_REQUEST_ID, requestId);
        networkReply.setProperty(PROP_HANDLER, 1);
        networkReply.setProperty(PROP_TIME_CREATED, QVariant::fromValue(getUnixTimestamp()));
        networkReply.setHttpStatusCode(429, "Too many requests");
        networkReply.setError(QNetworkReply::UnknownContentError, "429");
        QSignalSpy errorSignalSpy(&responseParser, &NetworkResponseParser::fireError);
        QSignalSpy rateLimitedSignalSpy(&responseParser, &NetworkResponseParser::rateLimited);

        //Act
        responseParser.parseNetworkError(&networkReply);

        //Assert
        REQUIRE(networkContext->lastSleepTime > 0);
        REQUIRE(errorSignalSpy.count() == 0);
        REQUIRE(rateLimitedSignalSpy.count() == 2);
        REQUIRE(rateLimitedSignalSpy.at(0).value(0).value<int>() == 0);
        REQUIRE(requester.timesInvoked() == 1);
        REQUIRE(requester.getLastRequestId() == requestId);
    }

    SECTION("When method is called with 503 response, retry request after errorCoolOffTime") {
        //Arrange
        const int requestId = 1;

        std::shared_ptr<NetworkContext> networkContext = std::make_shared<NetworkContext>();
        FakeNetworkRequester requester(networkContext);
        NetworkResponseParser responseParser(nullptr, networkContext, 10, 10, 0, 10);
        responseParser.setNetworkRequester(&requester);
        FakeQNetworkReply networkReply;
        networkReply.setProperty(PROP_REQUEST_ID, requestId);
        networkReply.setProperty(PROP_HANDLER, 1);
        networkReply.setProperty(PROP_TIME_CREATED, QVariant::fromValue(getUnixTimestamp()));
        networkReply.setHttpStatusCode(503, "Service unavailable");
        networkReply.setError(QNetworkReply::ServiceUnavailableError, "Service unavailable");
        QSignalSpy errorSignalSpy(&responseParser, &NetworkResponseParser::fireError);
        QSignalSpy rateLimitedSignalSpy(&responseParser, &NetworkResponseParser::rateLimited);

        //Act
        responseParser.parseNetworkError(&networkReply);

        //Assert
        REQUIRE(networkContext->lastSleepTime > 0);
        REQUIRE(errorSignalSpy.count() == 0);
        REQUIRE(rateLimitedSignalSpy.count() == 0);
        REQUIRE(requester.timesInvoked() == 1);
        REQUIRE(requester.getLastRequestId() == requestId);
    }

    SECTION("When method is called on 503 response twice for same request, retry request only the first time and fail second time") {
        //Arrange
        const int requestId = 1;

        std::shared_ptr<NetworkContext> networkContext = std::make_shared<NetworkContext>();
        FakeNetworkRequester requester(networkContext);
        NetworkResponseParser responseParser(nullptr, networkContext, 10, 10, 0, 10);
        responseParser.setNetworkRequester(&requester);
        FakeQNetworkReply networkReply;
        networkReply.setProperty(PROP_REQUEST_ID, requestId);
        networkReply.setProperty(PROP_HANDLER, 1);
        networkReply.setProperty(PROP_TIME_CREATED, QVariant::fromValue(getUnixTimestamp()));
        networkReply.setHttpStatusCode(503, "Service unavailable");
        networkReply.setError(QNetworkReply::ServiceUnavailableError, "Service unavailable");
        QSignalSpy errorSignalSpy(&responseParser, &NetworkResponseParser::fireError);
        QSignalSpy rateLimitedSignalSpy(&responseParser, &NetworkResponseParser::rateLimited);

        //Act
        responseParser.parseNetworkError(&networkReply);
        networkContext->lastSleepTime = 0;
        responseParser.parseNetworkError(&networkReply);

        //Assert
        REQUIRE(networkContext->lastSleepTime == 0);
        REQUIRE(errorSignalSpy.count() == 1);
        REQUIRE(rateLimitedSignalSpy.count() == 0);
        auto requestIdFromSignal = errorSignalSpy.at(0).value(1).value<int>();
        REQUIRE(requestIdFromSignal == requestId);
        REQUIRE(requester.timesInvoked() == 1);
        REQUIRE(requester.getLastRequestId() == requestId);
    }

    SECTION("When method is called on 503 twice for different requests, retry the request only the first time") {
        //Arrange
        const int requestId1 = 1;
        const int requestId2 = 2;

        std::shared_ptr<NetworkContext> networkContext = std::make_shared<NetworkContext>();
        FakeNetworkRequester requester(networkContext);
        NetworkResponseParser responseParser(nullptr, networkContext, 10, 10, 0, 10);
        responseParser.setNetworkRequester(&requester);
        FakeQNetworkReply networkReply;
        networkReply.setProperty(PROP_REQUEST_ID, requestId1);
        networkReply.setProperty(PROP_HANDLER, 1);
        networkReply.setProperty(PROP_TIME_CREATED, QVariant::fromValue(getUnixTimestamp()));
        networkReply.setHttpStatusCode(503, "Service unavailable");
        networkReply.setError(QNetworkReply::ServiceUnavailableError, "Service unavailable");
        QSignalSpy errorSignalSpy(&responseParser, &NetworkResponseParser::fireError);
        QSignalSpy rateLimitedSignalSpy(&responseParser, &NetworkResponseParser::rateLimited);

        //Act
        responseParser.parseNetworkError(&networkReply);
        networkReply.setProperty(PROP_REQUEST_ID, requestId2);
        responseParser.parseNetworkError(&networkReply);

        //Assert
        REQUIRE(networkContext->lastSleepTime > 0);
        REQUIRE(errorSignalSpy.count() == 0);
        REQUIRE(rateLimitedSignalSpy.count() == 0);
        REQUIRE(requester.timesInvoked() == 2);
        REQUIRE(requester.getLastRequestId() == requestId2);
    }

    SECTION("When method is called on 400 response, don't retry the request") {
        //Arrange
        const int requestId = 1;

        std::shared_ptr<NetworkContext> networkContext = std::make_shared<NetworkContext>();
        FakeNetworkRequester requester(networkContext);
        NetworkResponseParser responseParser(nullptr, networkContext, 10, 10, 10, 10);
        responseParser.setNetworkRequester(&requester);
        FakeQNetworkReply networkReply;
        networkReply.setProperty(PROP_REQUEST_ID, requestId);
        networkReply.setProperty(PROP_HANDLER, 1);
        networkReply.setProperty(PROP_TIME_CREATED, QVariant::fromValue(getUnixTimestamp()));
        networkReply.setHttpStatusCode(404, "Not found");
        networkReply.setError(QNetworkReply::ContentNotFoundError, "Not foud");
        QSignalSpy signalSpy(&responseParser, &NetworkResponseParser::fireError);

        //Act
        responseParser.parseNetworkError(&networkReply);

        //Assert
        REQUIRE(networkContext->lastSleepTime == 0);
        REQUIRE(signalSpy.count() == 1);
        auto requestIdFromSignal = signalSpy.at(0).value(1).value<int>();
        REQUIRE(requestIdFromSignal == requestId);
        REQUIRE(requester.timesInvoked() == 0);
    }
}

#endif //GITSTAT_NETWORKRESPONSEPARSERTEST_H
