#ifndef SYNCHRONIZEDLINKEDHASHMAPTEST_H
#define SYNCHRONIZEDLINKEDHASHMAPTEST_H

#include <catch2/catch_test_macros.hpp>
#include "include/network/SynchronizedLinkedHashMap.h"
#include <list>

TEST_CASE("SynchronizedLinkedHashMap::insert / get / erase", "[function]"){
    SECTION("When one element is inserted in empty SynchronizedLinkedHashMap, return value of  method get is that inserted value."){
        Network::SynchronizedLinkedHashMap<int, int> map;
        int key = 1;
        int value = 2;
        map.insert(key, value);
        int expectedValue = 2;

        int returnValue = map.get(key);

        REQUIRE(returnValue == expectedValue);
    }

    SECTION("When one key is inserted twice in empty SynchronizedLinkedHashMap, size of map is one"){
        Network::SynchronizedLinkedHashMap<int, int> map;
        int key = 1;
        int value = 2;
        map.insert(key, value);
        map.insert(key, value);
        int expectedValue = 1;

        int returnValue = map.getValues()->size();

        REQUIRE(returnValue == expectedValue);
    }

    SECTION("When is tried to get non-existed key from map, return value of get is default value"){
        Network::SynchronizedLinkedHashMap<int, int> map;
        int key = 2;
        int expectedValue = 0;

        int returnValue = map.get(5);

        REQUIRE(returnValue == expectedValue);
    }

    SECTION("When one existing key in SynchronizedLinkedHashMap with 3 elements is erased, return value of erase method is true"){
        Network::SynchronizedLinkedHashMap<int, int> map;
        map.insert(1, 5);
        map.insert(2, 6);
        map.insert(3, 7);

        bool expectedValue = true;

        int returnValue = map.erase(2);

        REQUIRE(returnValue == expectedValue);
    }

    SECTION("When one non-existing key in SynchronizedLinkedHashMap with 3 elements is erased, return value of erase method is false"){
        Network::SynchronizedLinkedHashMap<int, int> map;
        map.insert(1, 5);
        map.insert(2, 6);
        map.insert(3, 7);

        bool expectedValue = false;

        int returnValue = map.erase(5);

        REQUIRE(returnValue == expectedValue);
    }

    SECTION("Checking if inserted elements are in the order they are added"){
        Network::SynchronizedLinkedHashMap<int, int> map;
        map.insert(1, 5);
        map.insert(2, 6);
        map.insert(3, 7);

        std::list<int> expectedValue;
        expectedValue.push_back(5);
        expectedValue.push_back(6);
        expectedValue.push_back(7);
        std::shared_ptr<std::list<int>> returnVector = map.getValues();

        REQUIRE(*returnVector == expectedValue);
    }

}

#endif // SYNCHRONIZEDLINKEDHASHMAPTEST_H
