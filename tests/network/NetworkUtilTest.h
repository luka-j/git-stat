//
// Created by luka on 27.12.21..
//

#include <catch2/catch_test_macros.hpp>
#include "include/network/NetworkUtils.h"
#include "tests/helpers/FakeQNetworkReply.h"
#include "include/network/SynchronizedLinkedHashMap.h"
#include <QDir>

TEST_CASE("Network::parseLinkHeader", "[function]"){
    SECTION("When Network::parseLinkHeader is called with one param, parsed returned value is appropriate"){
        QByteArray header = QByteArray("<uri-reference>; param1=value1");
        QString expectedUriReference = "uri-reference";
        QString expectedLinkParamKey = "param1";
        QString expectedLinkParamValue = "value1";
        bool expectedValue = true;

        std::vector<std::shared_ptr<Network::LinkHeader>> returnValue = Network::parseLinkHeader(header);
        QString returnedUriReference = returnValue[0]->uriReference;
        QString returnedLinkParamKey;
        QString returnedLinkParamValue;
        for(auto &el : returnValue[0]->params){
            returnedLinkParamKey = el.first;
            returnedLinkParamValue = el.second;
        }
        bool returnedValue = expectedUriReference == returnedUriReference &&
                             expectedLinkParamKey == returnedLinkParamKey &&
                             expectedLinkParamValue == returnedLinkParamValue;

        REQUIRE(expectedValue == returnedValue);
    }

    SECTION("When Network::parseLinkHeader is called with one quoted param, parsed returned value is appropriate"){
        QByteArray header = QByteArray("<uri-reference>; param1=\"value1\"");
        QString expectedUriReference = "uri-reference";
        QString expectedLinkParamKey = "param1";
        QString expectedLinkParamValue = "value1";
        bool expectedValue = true;

        std::vector<std::shared_ptr<Network::LinkHeader>> returnValue = Network::parseLinkHeader(header);
        QString returnedUriReference = returnValue[0]->uriReference;
        QString returnedLinkParamKey;
        QString returnedLinkParamValue;
        for(auto &el : returnValue[0]->params){
            returnedLinkParamKey = el.first;
            returnedLinkParamValue = el.second;
        }
        bool returnedValue = expectedUriReference == returnedUriReference &&
                             expectedLinkParamKey == returnedLinkParamKey &&
                             expectedLinkParamValue == returnedLinkParamValue;

        REQUIRE(expectedValue == returnedValue);
    }

    SECTION("When Network::parseLinkHeader is called with two param, parsed returned value is appropriate"){
        QByteArray header = QByteArray("<uri-reference>; param1=value1; param2=value2");
        QString expectedUriReference = "uri-reference";
        std::vector<QString> expectedLinkParamKeys = {"param1", "param2"};
        std::vector<QString> expectedLinkParamValues = {"value1", "value2"};
        bool expectedValue = true;

        std::vector<std::shared_ptr<Network::LinkHeader>> returnValue = Network::parseLinkHeader(header);
        QString returnedUriReference = returnValue[0]->uriReference;
        std::vector<QString> returnedLinkParamKeys = {};
        std::vector<QString> returnedLinkParamValues = {};
        for(auto &el : returnValue[0]->params){
            returnedLinkParamKeys.push_back(el.first);
            returnedLinkParamValues.push_back(el.second);
        }
        bool returnedValue = expectedUriReference == returnedUriReference &&
                             expectedLinkParamKeys[0] == returnedLinkParamKeys[1] &&
                             expectedLinkParamValues[0] == returnedLinkParamValues[1] &&
                             expectedLinkParamKeys[1] == returnedLinkParamKeys[0] &&
                             expectedLinkParamValues[1] == returnedLinkParamValues[0];

        REQUIRE(expectedValue == returnedValue);
    }

    SECTION("When Network::parseLinkHeader is called with two quoted param, parsed returned value is appropriate"){
        QByteArray header = QByteArray("<uri-reference>; param1=\"value1\"; param2=\"value2\"");
        QString expectedUriReference = "uri-reference";
        std::vector<QString> expectedLinkParamKeys = {"param1", "param2"};
        std::vector<QString> expectedLinkParamValues = {"value1", "value2"};
        bool expectedValue = true;

        std::vector<std::shared_ptr<Network::LinkHeader>> returnValue = Network::parseLinkHeader(header);
        QString returnedUriReference = returnValue[0]->uriReference;
        std::vector<QString> returnedLinkParamKeys = {};
        std::vector<QString> returnedLinkParamValues = {};
        for(auto &el : returnValue[0]->params){
            returnedLinkParamKeys.push_back(el.first);
            returnedLinkParamValues.push_back(el.second);
        }
        bool returnedValue = expectedUriReference == returnedUriReference &&
                             expectedLinkParamKeys[0] == returnedLinkParamKeys[1] &&
                             expectedLinkParamValues[0] == returnedLinkParamValues[1] &&
                             expectedLinkParamKeys[1] == returnedLinkParamKeys[0] &&
                             expectedLinkParamValues[1] == returnedLinkParamValues[0];

        REQUIRE(expectedValue == returnedValue);
    }

    SECTION("When Network::parseLinkHeader is called with two refs, parsed returned value is appropriate"){
        QByteArray header = QByteArray("<uri-reference1>; param1=value1, <uri-reference2>; param2=value2");
        std::vector<QString> expectedUriReferences = {"uri-reference1", "uri-reference2"};
        std::vector<QString> expectedLinkParamKeys = {"param1", "param2"};
        std::vector<QString> expectedLinkParamValues = {"value1", "value2"};
        bool expectedValue = true;

        std::vector<std::shared_ptr<Network::LinkHeader>> returnValue = Network::parseLinkHeader(header);
        std::vector<QString> returnedUriReferences = {};
        std::vector<QString> returnedLinkParamKeys = {};
        std::vector<QString> returnedLinkParamValues = {};
        for(int i=0; i<returnValue.size(); i++){
            returnedUriReferences.push_back(returnValue[i]->uriReference);
            for(auto &el : returnValue[i]->params){
                returnedLinkParamKeys.push_back(el.first);
                returnedLinkParamValues.push_back(el.second);
            }
        }
        bool returnedValue = expectedUriReferences[0] == returnedUriReferences[0] &&
                             expectedUriReferences[1] == returnedUriReferences[1] &&
                             expectedLinkParamKeys[0] == returnedLinkParamKeys[0] &&
                             expectedLinkParamValues[0] == returnedLinkParamValues[0] &&
                             expectedLinkParamKeys[1] == returnedLinkParamKeys[1] &&
                             expectedLinkParamValues[1] == returnedLinkParamValues[1];


        REQUIRE(expectedValue == returnedValue);
    }
}

TEST_CASE("Network::loadAuthToken / Network::saveAuthToken", "[function]"){
    SECTION("When Network::loadAuthToken is called with parameter which is non-existig path, return value is empty string"){
        QString non_existing_file = "non-exitsting file";
        std::string expectedValue = "";

        std::string returnValue = Network::loadAuthToken(non_existing_file);

        REQUIRE(expectedValue == returnValue);
    }

    SECTION("When Network::loadAuthToken is called with parameter which is existig path, return value is authToken string"){
        std::string test_token = "test1 test2";
        QString existing_file = QDir::homePath().append("/").append("token_test");

        Network::saveAuthToken(test_token, existing_file);
        std::string returnValue = Network::loadAuthToken(existing_file);

        REQUIRE(test_token == returnValue);
    }
}

TEST_CASE("Network::makeQNetworkRequest", "[function]"){
    SECTION("When Network::makeQNetworkRequest is called with url parameter without '?',appropriate QNetworkRequest is created"){
        std::shared_ptr<QNetworkRequest> expectedObj = std::make_shared<QNetworkRequest>();
        std::string urlString = "http://www.company.com/news.html";
        QUrl url = QUrl(QString::fromStdString(urlString).append("?per_page=100"));
        expectedObj->setUrl(url);
        expectedObj->setAttribute(QNetworkRequest::HttpPipeliningAllowedAttribute, true);
        expectedObj->setRawHeader(QByteArray::fromStdString("PRIVATE-TOKEN"), QByteArray::fromStdString(""));
        bool expectedValue = true;

        std::shared_ptr<QNetworkRequest> returnedObj = Network::makeQNetworkRequest(urlString);
        bool returnedValue = expectedObj->url() == returnedObj->url() &&
                             expectedObj->attribute(QNetworkRequest::HttpPipeliningAllowedAttribute) == returnedObj->attribute(QNetworkRequest::HttpPipeliningAllowedAttribute) &&
                             expectedObj->rawHeader(QByteArray::fromStdString("PRIVATE-TOKEN")) == returnedObj->rawHeader(QByteArray::fromStdString("PRIVATE-TOKEN"));

        REQUIRE(expectedValue == returnedValue);
    }

    SECTION("When Network::makeQNetworkRequest is called with url parameter with '?',appropriate QNetworkRequest is created"){
        std::shared_ptr<QNetworkRequest> expectedObj = std::make_shared<QNetworkRequest>();
        std::string urlString = "http://www.company.com/news.html?user=Alice&year=2008";
        QUrl url = QUrl(QString::fromStdString(urlString));
        expectedObj->setUrl(url);
        expectedObj->setAttribute(QNetworkRequest::HttpPipeliningAllowedAttribute, true);
        expectedObj->setRawHeader(QByteArray::fromStdString("PRIVATE-TOKEN"), QByteArray::fromStdString(""));
        bool expectedValue = true;

        std::shared_ptr<QNetworkRequest> returnedObj = Network::makeQNetworkRequest(urlString);
        bool returnedValue = expectedObj->url() == returnedObj->url() &&
                             expectedObj->attribute(QNetworkRequest::HttpPipeliningAllowedAttribute) == returnedObj->attribute(QNetworkRequest::HttpPipeliningAllowedAttribute) &&
                             expectedObj->rawHeader(QByteArray::fromStdString("PRIVATE-TOKEN")) == returnedObj->rawHeader(QByteArray::fromStdString("PRIVATE-TOKEN"));

        REQUIRE(expectedValue == returnedValue);
    }

    SECTION("When Network::makeQNetworkRequest is called with non-empty authToken,appropriate QNetworkRequest is created"){
        std::shared_ptr<QNetworkRequest> expectedObj = std::make_shared<QNetworkRequest>();
        std::string authToken = "test1 test2";
        std::string urlString = "http://www.company.com/news.html";
        QUrl url = QUrl(QString::fromStdString(urlString).append("?per_page=100"));
        expectedObj->setUrl(url);
        expectedObj->setAttribute(QNetworkRequest::HttpPipeliningAllowedAttribute, true);
        expectedObj->setRawHeader(QByteArray::fromStdString("PRIVATE-TOKEN"), QByteArray::fromStdString(authToken));
        bool expectedValue = true;

        std::shared_ptr<QNetworkRequest> returnedObj = Network::makeQNetworkRequest(urlString, authToken);
        bool returnedValue = expectedObj->url() == returnedObj->url() &&
                             expectedObj->attribute(QNetworkRequest::HttpPipeliningAllowedAttribute) == returnedObj->attribute(QNetworkRequest::HttpPipeliningAllowedAttribute) &&
                             expectedObj->rawHeader(QByteArray::fromStdString("PRIVATE-TOKEN")) == returnedObj->rawHeader(QByteArray::fromStdString("PRIVATE-TOKEN"));

        REQUIRE(expectedValue == returnedValue);
    }
}




