//
// Created by caca on 28.12.21..
//

#ifndef GITSTAT_COMMITPROCESSINGTEST_H
#define GITSTAT_COMMITPROCESSINGTEST_H

#include <catch2/catch_test_macros.hpp>
#include <QJsonObject>
#include <QJsonDocument>
#include "../../include/data/ProcessingCommit.h"
#include "../../include/network/GitLabCommitResponse.h"
#include <memory>


TEST_CASE("ProcessingCommit::commitFromQJsonToClass","[function]"){

    SECTION("If QJsonObject has all properties, function returns QSharedPointer<Commit> with same values"){

        QJsonObject author {
            {"name", "Author"},
            {"email", "author@gmail.com"}
        };
        QJsonObject stats {
            {"additions", 5},
            {"deletions", 1},
            {"total", 6}
        };
        QJsonObject changes1 {
            {"oldFilename", "oldFile"},
            {"newFilename", "newFile"},
            {"isFileCreated", false},
            {"isFileRenamed", true},
            {"isFileDeleted", false}
        };
        QJsonArray changes;
        changes.append(changes1);

        QJsonObject input  {
            {"id", "ausiadnasfaf5as4f5afa"},
            {"author",author},
            {"createdAt","2021-12-27T16:30:18"},
            {"title","Commit 1"},
            {"message","Message 1"},
            {"webUrl","www.url.com"},
            {"stats",stats},
            {"changes",changes}
        };
        Commit::CommitStats commStats;
        commStats.additions=5;
        commStats.deletions=1;
        commStats.total=6;
        QSharedPointer<Commit> expected = QSharedPointer<Commit>::create("ausiadnasfaf5as4f5afa",QDateTime::fromString("2021-12-27T16:30:18",Qt::ISODate),"Commit 1","Message 1",
                                                                         Author("Author","author@gmail.com"),"www.url.com",commStats, QVector<Commit::FileChange>{Commit::FileChange("oldFile","newFile",false,true,false)});

        ProcessingCommit *pc = ProcessingCommit::getInstance();
        QSharedPointer<Commit> returnValue = pc->commitFromQJsonToClass(input);

        REQUIRE(*expected.data() == *returnValue.data());
    }
}

TEST_CASE("ProcessingCommit::commitFromClassToQJson","[function]"){

    SECTION("For given QSharedPointer<Commit> function returns QJsonObject with same values"){

        Commit::CommitStats commStats;
        commStats.additions=5;
        commStats.deletions=1;
        commStats.total=6;
        QSharedPointer<Commit> input = QSharedPointer<Commit>::create("asjaflskamlfad758asf",QDateTime::fromString("2021-12-13T13:56:15",Qt::ISODate),"Commit 1",
                                                                      "Message 1",Author("Aleksandra","email@gmail.com"),"www.url.com",commStats, QVector<Commit::FileChange>{Commit::FileChange("oldFile","newFile",false,true,false)});
        QJsonObject author {
            {"name", "Aleksandra"},
            {"email", "email@gmail.com"}
        };

        QJsonObject stats {
            {"additions", 5},
            {"deletions", 1},
            {"total", 6}
        };
        QJsonObject changes1 {
            {"oldFilename", "oldFile"},
            {"newFilename", "newFile"},
            {"isFileCreated", false},
            {"isFileRenamed", true},
            {"isFileDeleted", false}
        };
        QJsonArray changes;
        changes.append(changes1);

        QJsonObject expected {
            {"author",author},
            {"changes",changes},
            {"createdAt","2021-12-13T13:56:15"},
            {"id", "asjaflskamlfad758asf"},
            {"message","Message 1"},
            {"stats",stats},
            {"title","Commit 1"},
            {"webUrl","www.url.com"}
        };

        ProcessingCommit *pc = ProcessingCommit::getInstance();
        QJsonObject returnValue = pc->commitFromClassToQJson(input);

        REQUIRE(returnValue == expected);
    }
}

TEST_CASE("ProcessingCommit::createQJsonFromCommitMap","[function]"){

    SECTION("If map is empty, function returns empty QJsonArray"){
        QSharedPointer<QMap<Author,QVector<QSharedPointer<Commit>>>>input = QSharedPointer<QMap<Author,QVector<QSharedPointer<Commit>>>>::create();
        int expectedSize = 0;

        ProcessingCommit *pc = ProcessingCommit::getInstance();
        QJsonArray returnValue = pc->createQJsonFromCommitMap(input);
        int returnSize = returnValue.size();

        REQUIRE(expectedSize == returnSize);
    }

    SECTION("If map has 1 element, function returns QJsonArray of size 1"){
        QSharedPointer<QMap<Author,QVector<QSharedPointer<Commit>>>> input = QSharedPointer<QMap<Author,QVector<QSharedPointer<Commit>>>>::create();
        Commit::CommitStats commStats;
        commStats.additions=5;
        commStats.deletions=1;
        commStats.total=6;
        QVector<QSharedPointer<Commit>> commits;
        commits.append(QSharedPointer<Commit>::create("ausiadnasfaf5as4f5afa",QDateTime::fromString("2021-12-27T16:30:18",Qt::ISODate),"Commit 1","Message 1",
                                                      Author("Author","author@gmail.com"),"www.url.com",commStats, QVector<Commit::FileChange>{Commit::FileChange("oldFile","newFile",false,true,false)}));
        input->insert(Author("Author","author@gmail.com"),commits);
        int expectedSize = 1;

        ProcessingCommit *pc = ProcessingCommit::getInstance();
        QJsonArray returnValue = pc->createQJsonFromCommitMap(input);
        int returnSize = returnValue.size();

        REQUIRE(expectedSize == returnSize);
    }
}

TEST_CASE("ProcessingCommit::createCommitMapFromQJson","[function]"){

    SECTION("If QJsonArray is empty, function returns empty map"){
        QJsonArray input;
        int expectedSize = 0;

        ProcessingCommit *pc = ProcessingCommit::getInstance();
        QSharedPointer<QMap<Author,QVector<QSharedPointer<Commit>>>> returnValue = pc->createCommitMapFromQJson(input,31055519);
        int returnSize = returnValue->size();

        REQUIRE(expectedSize == returnSize);
    }

    SECTION("If QJsonArray has 1 element (1 author, 2 commits), function returns map of size 1"){
        QJsonObject author {
            {"name", "Author"},
            {"email", "author@gmail.com"}
        };
        QJsonObject stats1 {
            {"additions", 5},
            {"deletions", 1},
            {"total", 6}
        };
        QJsonObject changes11 {
            {"oldFilename", "oldFile"},
            {"newFilename", "newFile"},
            {"isFileCreated", false},
            {"isFileRenamed", true},
            {"isFileDeleted", false}
        };

        QJsonArray changes1;
        changes1.append(changes11);

        QJsonObject stats2 {
            {"additions", 7},
            {"deletions", 5},
            {"total", 12}
        };
        QJsonObject changes22 {
            {"oldFilename", "File1"},
            {"newFilename", "File2"},
            {"isFileCreated", false},
            {"isFileRenamed", true},
            {"isFileDeleted", true}
        };

        QJsonArray changes2;
        changes2.append(changes22);

        QJsonObject commit1 {
            {"author",author},
            {"changes",changes1},
            {"createdAt","2021-12-13T13:56:15"},
            {"id", "asjaflskamlfad758asf"},
            {"message","Message 1"},
            {"stats",stats1},
            {"title","Commit 1"},
            {"webUrl","www.url1.com"}
        };
        QJsonObject commit2 {
            {"author",author},
            {"changes",changes2},
            {"createdAt","2021-12-27T16:30:18"},
            {"id", "ausiadnasfaf5as4f5afa"},
            {"message","Message 1"},
            {"stats",stats2},
            {"title","Commit 2"},
            {"webUrl","www.url2.com"}
        };

        QJsonArray commits;
        commits.append(commit1);
        commits.append(commit2);

        QJsonObject obj {
            {"author",author},
            {"commits",commits}
        };

        QJsonArray input;
        input.append(obj);

        int expectedSize = 1;

        ProcessingCommit *pc = ProcessingCommit::getInstance();
        QSharedPointer<QMap<Author,QVector<QSharedPointer<Commit>>>> returnValue = pc->createCommitMapFromQJson(input,31055519);
        int returnSize = returnValue->size();

        REQUIRE(expectedSize == returnSize);
    }

    SECTION("If QJsonArray has 1 element (1 author, 1 commit), function returns map of size 1"){
        QJsonObject author {
            {"name", "Author"},
            {"email", "author@gmail.com"}
        };
        QJsonObject stats {
            {"additions", 5},
            {"deletions", 1},
            {"total", 6}
        };
        QJsonObject changes1 {
            {"oldFilename", "oldFile"},
            {"newFilename", "newFile"},
            {"isFileCreated", false},
            {"isFileRenamed", true},
            {"isFileDeleted", false}
        };

        QJsonArray changes;
        changes.append(changes1);

        QJsonObject commit {
            {"author",author},
            {"changes",changes},
            {"createdAt","2021-12-13T13:56:15"},
            {"id", "asjaflskamlfad758asf"},
            {"message","Message 1"},
            {"stats",stats},
            {"title","Commit 1"},
            {"webUrl","www.url.com"}
        };

        QJsonArray commits;
        commits.append(commit);

        QJsonObject obj {
            {"author",author},
            {"commits",commits}
        };

        QJsonArray input;
        input.append(obj);

        int expectedSize = 1;

        ProcessingCommit *pc = ProcessingCommit::getInstance();
        QSharedPointer<QMap<Author,QVector<QSharedPointer<Commit>>>> returnValue = pc->createCommitMapFromQJson(input,31055519);
        int returnSize = returnValue->size();

        REQUIRE(expectedSize == returnSize);
    }

    SECTION("If QJsonArray has 3 commmits with different authors, function returns map of size 3"){
        QJsonObject author1 {
            {"name", "Author1"},
            {"email", "author1@gmail.com"}
        };
        QJsonObject author2 {
            {"name", "Author1"},
            {"email", "author2@gmail.com"}
        };
        QJsonObject author3 {
            {"name", "Author1"},
            {"email", "author3@gmail.com"}
        };
        QJsonObject stats1 {
            {"additions", 5},
            {"deletions", 1},
            {"total", 6}
        };
        QJsonObject changes11 {
            {"oldFilename", "oldFile"},
            {"newFilename", "newFile"},
            {"isFileCreated", false},
            {"isFileRenamed", true},
            {"isFileDeleted", false}
        };
        QJsonArray changes1;
        changes1.append(changes11);

        QJsonObject stats2 {
            {"additions", 7},
            {"deletions", 5},
            {"total", 12}
        };
        QJsonObject changes22 {
            {"oldFilename", "File1"},
            {"newFilename", "File2"},
            {"isFileCreated", false},
            {"isFileRenamed", true},
            {"isFileDeleted", true}
        };
        QJsonArray changes2;
        changes2.append(changes22);

        QJsonObject stats3 {
            {"additions", 2},
            {"deletions", 5},
            {"total", 7}
        };
        QJsonObject changes33 {
            {"oldFilename", "File3"},
            {"newFilename", "File4"},
            {"isFileCreated", true},
            {"isFileRenamed", false},
            {"isFileDeleted", true}
        };
        QJsonArray changes3;
        changes3.append(changes33);

        QJsonObject commit1 {
            {"author",author1},
            {"changes",changes1},
            {"createdAt","2021-12-13T13:56:15"},
            {"id", "asjaflskamlfad758asf"},
            {"message","Message 1"},
            {"stats",stats1},
            {"title","Commit 1"},
            {"webUrl","www.url1.com"}
        };
        QJsonObject commit2 {
            {"author",author2},
            {"changes",changes2},
            {"createdAt","2021-12-27T16:30:18"},
            {"id", "ausiadnasfaf5as4f5afa"},
            {"message","Message 2"},
            {"stats",stats2},
            {"title","Commit 2"},
            {"webUrl","www.url2.com"}
        };
        QJsonObject commit3 {
            {"author",author3},
            {"changes",changes3},
            {"createdAt","2021-11-07T11:06:10"},
            {"id", "as7aysf6sa7f8a7fsa87f"},
            {"message","Message 3"},
            {"stats",stats3},
            {"title","Commit 3"},
            {"webUrl","www.url3.com"}
        };
        QJsonArray commits1;
        commits1.append(commit1);

        QJsonArray commits2;
        commits2.append(commit2);

        QJsonArray commits3;
        commits3.append(commit3);

        QJsonObject obj1 {
            {"author",author1},
            {"commits",commits1}
        };
        QJsonObject obj2 {
            {"author",author2},
            {"commits",commits2}
        };
        QJsonObject obj3 {
            {"author",author3},
            {"commits",commits3}
        };

        QJsonArray input;
        input.append(obj1);
        input.append(obj2);
        input.append(obj3);

        int expectedSize = 3;

        ProcessingCommit *pc = ProcessingCommit::getInstance();
        QSharedPointer<QMap<Author,QVector<QSharedPointer<Commit>>>> returnValue = pc->createCommitMapFromQJson(input,31055519);
        int returnSize = returnValue->size();

        REQUIRE(expectedSize == returnSize);
    }

    SECTION("If QJsonArray has 3 commmits with 2 different authors, function returns map of size 2"){
        QJsonObject author1 {
            {"name", "Author1"},
            {"email", "author1@gmail.com"}
        };
        QJsonObject author2 {
            {"name", "Author1"},
            {"email", "author2@gmail.com"}
        };
        QJsonObject stats1 {
            {"additions", 5},
            {"deletions", 1},
            {"total", 6}
        };
        QJsonObject changes11 {
            {"oldFilename", "oldFile"},
            {"newFilename", "newFile"},
            {"isFileCreated", false},
            {"isFileRenamed", true},
            {"isFileDeleted", false}
        };
        QJsonArray changes1;
        changes1.append(changes11);

        QJsonObject stats2 {
            {"additions", 7},
            {"deletions", 5},
            {"total", 12}
        };
        QJsonObject changes22 {
            {"oldFilename", "File1"},
            {"newFilename", "File2"},
            {"isFileCreated", false},
            {"isFileRenamed", true},
            {"isFileDeleted", true}
        };
        QJsonArray changes2;
        changes2.append(changes22);

        QJsonObject stats3 {
            {"additions", 2},
            {"deletions", 5},
            {"total", 7}
        };
        QJsonObject changes33 {
            {"oldFilename", "File3"},
            {"newFilename", "File4"},
            {"isFileCreated", true},
            {"isFileRenamed", false},
            {"isFileDeleted", true}
        };
        QJsonArray changes3;
        changes3.append(changes33);

        QJsonObject commit1 {
            {"author",author1},
            {"changes",changes1},
            {"createdAt","2021-12-13T13:56:15"},
            {"id", "asjaflskamlfad758asf"},
            {"message","Message 1"},
            {"stats",stats1},
            {"title","Commit 1"},
            {"webUrl","www.url1.com"}
        };
        QJsonObject commit2 {
            {"author",author1},
            {"changes",changes2},
            {"createdAt","2021-12-27T16:30:18"},
            {"id", "ausiadnasfaf5as4f5afa"},
            {"message","Message 2"},
            {"stats",stats2},
            {"title","Commit 2"},
            {"webUrl","www.url2.com"}
        };
        QJsonObject commit3 {
            {"author",author2},
            {"changes",changes3},
            {"createdAt","2021-11-07T11:06:10"},
            {"id", "as7aysf6sa7f8a7fsa87f"},
            {"message","Message 3"},
            {"stats",stats3},
            {"title","Commit 3"},
            {"webUrl","www.url3.com"}
        };
        QJsonArray commits1;
        commits1.append(commit1);
        commits1.append(commit2);

        QJsonArray commits2;
        commits2.append(commit3);

        QJsonObject obj1 {
            {"author",author1},
            {"commits",commits1}
        };
        QJsonObject obj2 {
            {"author",author2},
            {"commits",commits2}
        };

        QJsonArray input;
        input.append(obj1);
        input.append(obj2);

        int expectedSize = 2;

        ProcessingCommit *pc = ProcessingCommit::getInstance();
        QSharedPointer<QMap<Author,QVector<QSharedPointer<Commit>>>> returnValue = pc->createCommitMapFromQJson(input,31055519);
        int returnSize = returnValue->size();

        REQUIRE(expectedSize == returnSize);
    }
}

TEST_CASE("ProcessingCommit::parseToCommitCommitNetworkResponse","[function]"){
    SECTION("Function will return QSharedPointer<Commit> with same values that CommitNetworkResponse returned"){
        QJsonObject input = QJsonObject();

        std::shared_ptr<Network::GitLabCommitResponse> networkResponse = std::make_shared<Network::GitLabCommitResponse>(input);
        networkResponse->id="12345678";
        networkResponse->shortId= "123";
        networkResponse->title = "Title";
        networkResponse->createdAt = "2021-09-20T11:50:22+03:00";
        networkResponse->parentIds = std::vector<std::string>{"111", "222"} ;
        networkResponse->message = "Message 1";
        networkResponse->authorName = "Author1";
        networkResponse->authorEmail = "author1@gmail.com";
        networkResponse->authoredDate = "2021-10-20T11:50:22+03:00";
        networkResponse->committerName = "Example User";
        networkResponse->committerEmail = "author2@gmail.com";
        networkResponse->committedDate = "2021-10-20T11:50:22+03:00";
        networkResponse->webUrl = "www.url.com";
        Network::GitLabCommitResponse::CommitStats stats;
        stats.additions = 5;
        stats.deletions = 1;
        stats.total = 6;
        networkResponse->stats = stats;
        std::shared_ptr<Network::GitLabCommitResponse::FileChange> changes  = std::make_shared<Network::GitLabCommitResponse::FileChange>(input) ;
        changes->oldFilename="oldName";
        changes->newFilename = "newName";
        changes->isFileDeleted=false;
        changes->isFileRenamed = true;
        changes->isFileCreated = false;
        networkResponse->fileChanges = std::vector<std::shared_ptr<Network::GitLabCommitResponse::FileChange>>{changes};
        Commit::CommitStats comStats;
        comStats.additions=5;
        comStats.deletions=1;
        comStats.total=6;
        QVector<Commit::FileChange> comChanges;
        Commit::FileChange com("oldName","newName",false,true,false);
        comChanges.append(com);
        QSharedPointer<Commit> expected = QSharedPointer<Commit>::create("12345678", QDateTime::fromString("2021-09-20T11:50:22+03:00",Qt::ISODate).toLocalTime(),"Title", "Message 1",
                                                                             Author("author1", "author1@gmail.com"),"www.url.com", comStats, comChanges);
        ProcessingCommit *commitProcessor = ProcessingCommit::getInstance();
        QSharedPointer<Commit> returnValue = commitProcessor->parseToCommitCommitNetworkResponse(123456789, networkResponse);

        REQUIRE(*expected.data() == *returnValue.data());
    }
}



#endif //GITSTAT_COMMITPROCESSINGTEST_H
