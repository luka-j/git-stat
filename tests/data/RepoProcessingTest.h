//
// Created by caca on 28.12.21..
//

#ifndef GITSTAT_REPOPROCESSINGTEST_H
#define GITSTAT_REPOPROCESSINGTEST_H


#include <catch2/catch_test_macros.hpp>
#include <QJsonObject>
#include <QJsonDocument>
#include <QList>
#include <QDateTime>
#include <memory>
#include <vector>
#include <string>
#include <iostream>
#include <QFileInfo>

#include "../../include/data/ProcessingRepo.h"
#include "../../include/network/GitLabProjectResponse.h"
#include "../../include/data/Repo.h"


TEST_CASE("ProcessingRepo::readAllReposFromDisk","[function]"){
    SECTION("If file doesn't exist, function will return empty vector"){
        QString inputFile = "fake.json";

        QFile repos(inputFile);
        QFileInfo checkFile(inputFile);
        if(checkFile.exists() && checkFile.isFile()){
            repos.remove();
        }
        
        int expected = 0;

        ProcessingRepo *pr = ProcessingRepo::getInstance();
        QSharedPointer<QVector<QSharedPointer<Repo>>> returnValue = pr->readAllReposFromDisk(inputFile);
        int returnSize = returnValue->size();

        REQUIRE(expected == returnSize);
    }

    SECTION("If file exists, the size of returned vector of repos is same as number of QJsonObjects in file") {
        QString pathInput = "fakeRepos.json";
        QFile repos(pathInput);
        repos.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::Text);
        QTextStream stream(&repos);
        stream << "[\n {\n"
            "                   \"commitsPerAuthor\": [], "
            "                   \"createdAt\": \"2021-11-04T20:02:21\",\n"
            "                   \"description\": \"\",\n"
            "                   \"groupId\": 14033104,\n"
            "                    \"groupName\": \"Projects 2021-2022\",\n"
            "                    \"id\": 31055519,\n"
            "                    \"lastActivity\": \"2021-12-27T15:44:32\",\n"
            "                    \"name\": \"MATF / course-rs / Projects 2021-2022 / 13-Git-stat\",\n"
            "                    \"numOfAuthors\": 0,\n"
            "                    \"numOfCommits\": 0,\n"
            "                    \"webUrl\": \"https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/13-Git-stat\"\n"
            "        }\n]";
        repos.close();
        int expected = 1;

        ProcessingRepo *pr = ProcessingRepo::getInstance();
        int returnSize = pr->readAllReposFromDisk(pathInput)->size();

        REQUIRE(expected == returnSize);
    }
}

TEST_CASE("ProcessingRepo::writeAllReposToDisk","[function]"){
    SECTION("If file doesn't exist, it will be created"){
        QString fileName = "notExistingFile.json";
        QFile reposFile(fileName);
        QFileInfo checkInfo(fileName);
        if(checkInfo.exists() && checkInfo.isFile()){
            reposFile.remove();
        }
        QSharedPointer<Repo> repo1 = QSharedPointer<Repo>::create(1,"repo1", QDateTime::fromString("2020-12-27T22:15:15",Qt::ISODate),"www.repo1.com","",QDateTime::fromString("2020-12-29T22:15:15",Qt::ISODate));
        QSharedPointer<Repo> repo2 = QSharedPointer<Repo>::create(2,"repo2", QDateTime::fromString("2020-12-27T22:15:15",Qt::ISODate),"www.repo2.com","",QDateTime::fromString("2020-12-29T22:15:15",Qt::ISODate));
        QVector<QSharedPointer<Repo>> repos{repo1,repo2};
        QSharedPointer<QVector<QSharedPointer<Repo>>> inputRepos = QSharedPointer<QVector<QSharedPointer<Repo>>>::create(repos);
        int expected = true;

        ProcessingRepo *pr = ProcessingRepo::getInstance();
        pr->writeAllReposToDisk(inputRepos, fileName);

        QFile fileCreated(fileName);
        int returnValue = fileCreated.exists();

        REQUIRE(expected == returnValue);

    }

    SECTION("File will contain as many repos as there are in vector from which we are writing"){
        QString inputPath = "fakeRepo.json";
        QSharedPointer<Repo> repo1 = QSharedPointer<Repo>::create(1,"repo1", QDateTime::fromString("2020-12-27T22:15:15",Qt::ISODate),"www.repo1.com","",QDateTime::fromString("2020-12-29T22:15:15",Qt::ISODate));
        QSharedPointer<Repo> repo2 = QSharedPointer<Repo>::create(2,"repo2", QDateTime::fromString("2020-12-27T22:15:15",Qt::ISODate),"www.repo2.com","",QDateTime::fromString("2020-12-29T22:15:15",Qt::ISODate));
        QVector<QSharedPointer<Repo>> repos{repo1,repo2};
        QSharedPointer<QVector<QSharedPointer<Repo>>> inputRepos = QSharedPointer<QVector<QSharedPointer<Repo>>>::create(repos);
        int expected = 2;

        ProcessingRepo *pr = ProcessingRepo::getInstance();
        pr->writeAllReposToDisk(inputRepos, inputPath);

        QFile myFile(inputPath);
        myFile.open(QIODevice::OpenModeFlag::ReadOnly | QIODevice::Text);
        QByteArray readValue = myFile.readAll();
        myFile.close();

        int writenSize = QJsonDocument::fromJson(readValue).array().size();

        REQUIRE(expected == writenSize);
    }
}

TEST_CASE("ProcessingRepo::parseRepoFromQJsonToClass","[function]"){

    SECTION("When QJsonObject has every value expect for commitsPerAuthor, function returns QSharedPointer<Repo> with the same values and empty commitsPerAuthor map"){
        QJsonArray commits;
        QJsonObject input {
            {"commitsPerAuthor",commits},
            {"createdAt","2021-11-04T20:02:21"},
            {"description",""},
            {"groupId",-1},
            {"groupName",""},
            {"id",1233456},
            {"lastActivity","2021-12-25T14:08:49"},
            {"name","Repo 1"},
            {"numOfAuthors",0},
            {"numOfCommits",0},
            {"webUrl","www.url.com"}
        };

        QSharedPointer<Repo> expected = QSharedPointer<Repo>::create(1233456,"Repo 1",
        QDateTime::fromString("2021-11-04T20:02:21",Qt::ISODate),"www.url.com","",
        QDateTime::fromString("2021-12-25T14:08:49",Qt::ISODate));

        ProcessingRepo *pr = ProcessingRepo::getInstance();
        QSharedPointer<Repo> returnValue = pr->parseRepoFromQJsonToClass(input);

        REQUIRE(*expected.data() == *returnValue.data());
    }

    SECTION("For QJsonObject, function returns QSharedPointer<Repo> with the same values and same number of commits"){

        QJsonObject author {
            {"name", "Author"},
            {"email", "author@gmail.com"}
        };
        QJsonObject stats {
            {"additions", 5},
            {"deletions", 1},
            {"total", 6}
        };
        QJsonObject changes1 {
            {"oldFilename", "oldFile"},
            {"newFilename", "newFile"},
            {"isFileCreated", false},
            {"isFileRenamed", false},
            {"isFileDeleted", false}
        };
        QJsonArray changes;
        changes.append(changes1);

        QJsonObject commit  {
            {"id", "ausiadnasfaf5as4f5afa"},
            {"author",author},
            {"createdAt","2021-12-27T16:30:18"},
            {"title","Commit 1"},
            {"message","Message 1"},
            {"webUrl","www.url.com"},
            {"stats",stats},
            {"changes",changes}
        };

        QJsonArray commits;
        commits.append(commit);

        QJsonObject o1 {
            {"author",author},
            {"commits",commits}
        };

        QJsonArray comm;
        comm.append(o1);

        QJsonObject input {
            {"commitsPerAuthor",comm},
            {"createdAt","2021-11-04T20:02:21"},
            {"description",""},
            {"groupId",-1},
            {"groupName",""},
            {"id",1233456},
            {"lastActivity","2021-12-25T14:08:49"},
            {"name","Repo 1"},
            {"numOfAuthors",1},
            {"numOfCommits",1},
            {"webUrl","www.url.com"}
        };
        Commit::CommitStats commStats;
        commStats.additions=5;
        commStats.deletions=1;
        commStats.total=6;
        QSharedPointer<Commit> expectedCommit = QSharedPointer<Commit>::create("ausiadnasfaf5as4f5afa",QDateTime::fromString("2021-12-27T16:30:18",Qt::ISODate),
                                                                         "Commit 1","Message 1",Author("Author","author@gmail.com"),"www.url.com",commStats,
                                                                         QVector<Commit::FileChange>{Commit::FileChange("oldFile","newFile",false,false,false)});

        QSharedPointer<QMap<Author,QVector<QSharedPointer<Commit>>>>expectedMap = QSharedPointer<QMap<Author,QVector<QSharedPointer<Commit>>>>::create();
        expectedMap->insert(Author("Author", "author@gmail.com"),QVector<QSharedPointer<Commit>>{expectedCommit});

        QSharedPointer<Repo> expected = QSharedPointer<Repo>::create(1233456,"Repo 1",
        QDateTime::fromString("2021-11-04T20:02:21",Qt::ISODate),"www.url.com","",
        QDateTime::fromString("2021-12-25T14:08:49",Qt::ISODate),1,1,expectedMap);

        int expectedSizeOfCommits = 1;

        ProcessingRepo *pr = ProcessingRepo::getInstance();
        QSharedPointer<Repo> returnValue = pr->parseRepoFromQJsonToClass(input);
        int returnedSizeOfCommits = returnValue.data()->getCommitsPerAuthor()->size();

        CHECK(expectedSizeOfCommits == returnedSizeOfCommits);
        REQUIRE(*expected.data() == *returnValue.data());
    }
}

TEST_CASE("ProcessingRepo::parseRepoFromClassToQJson","[function]"){
    SECTION("For given QSharedPointer<Repo> function returns QJsonObject with same values"){
        QSharedPointer<Repo> inputRepo = QSharedPointer<Repo>::create(31055519,"Repo 1",QDateTime::fromString("2021-11-04T20:02:21",Qt::ISODate),"www.url.com","",QDateTime::fromString("2021-12-25T14:08:49",Qt::ISODate));

        QJsonArray commits;
        QJsonObject expected {
            {"commitsPerAuthor",commits},
            {"createdAt","2021-11-04T20:02:21"},
            {"description",""},
            {"groupId",-1},
            {"groupName",""},
            {"id",31055519},
            {"lastActivity","2021-12-25T14:08:49"},
            {"name","Repo 1"},
            {"numOfAuthors",0},
            {"numOfCommits",0},
            {"webUrl","www.url.com"}
        };

        ProcessingRepo *pr = ProcessingRepo::getInstance();
        QJsonObject returnValue = pr->parseRepoFromClassToQJson(inputRepo);

        REQUIRE(expected == returnValue);
    }
}

TEST_CASE("ProcessingRepo::setCommitStatsForRepo","[function]"){
    SECTION("For given repo and map of 1 author with 1 commit, function sets numberOfAuthors and numberOfCommits on 1"){

        QSharedPointer<Repo> inputRepo = QSharedPointer<Repo>::create(31055519,"Repo 1",QDateTime::fromString("2021-11-04T20:02:21",Qt::ISODate),"www.url.com","",QDateTime::fromString("2021-12-25T14:08:49",Qt::ISODate));
        QSharedPointer<QMap<Author, QVector<QSharedPointer<Commit>>>> inputMap = QSharedPointer<QMap<Author,QVector<QSharedPointer<Commit>>>>::create();
        Commit::CommitStats commStats;
        commStats.additions=5;
        commStats.deletions=1;
        commStats.total=6;
        QSharedPointer<Commit> inputCommit = QSharedPointer<Commit>::create("ausiadnasfaf5as4f5afa",QDateTime::fromString("2021-12-27T16:30:18",Qt::ISODate),"Commit 1","Message 1",Author("Aleksandra","email@gmail.com"),"www.face.com",commStats, QVector<Commit::FileChange>{Commit::FileChange("oldFile","newFile",false,true,false)});
        QVector<QSharedPointer<Commit>> commits;
        commits.append(inputCommit);
        inputMap->insert(Author("Author1","author1@gmail.com"),commits);
        int expectedNumOfCommits = 1;
        int expectedNumOfAuthors = 1;

        ProcessingRepo *pr = ProcessingRepo::getInstance();
        pr->setCommitStatsForRepo(inputRepo,inputMap);
        int returnNumOfCommits = inputRepo->getNumOfCommits();
        int returnNumOfAuthors = inputRepo->getNumOfAuthors();

        CHECK(returnNumOfCommits == expectedNumOfCommits);
        REQUIRE(returnNumOfAuthors == expectedNumOfAuthors);
    }

    SECTION("For given repo and map of 2 author with 1 and 2 commits, function sets numberOfAuthors on 2 and numberOfCommits on 3"){
        QSharedPointer<Repo> inputRepo = QSharedPointer<Repo>::create(31055519,"Repo 1",QDateTime::fromString("2021-11-04T20:02:21",Qt::ISODate),"www.url.com","",QDateTime::fromString("2021-12-25T14:08:49",Qt::ISODate));
        QSharedPointer<QMap<Author, QVector<QSharedPointer<Commit>>>> inputMap = QSharedPointer<QMap<Author,QVector<QSharedPointer<Commit>>>>::create();
        Commit::CommitStats commStats1;
        commStats1.additions=5;
        commStats1.deletions=1;
        commStats1.total=6;
        Commit::CommitStats commStats2;
        commStats2.additions=3;
        commStats2.deletions=7;
        commStats2.total=10;
        Commit::CommitStats commStats3;
        commStats3.additions=2;
        commStats3.deletions=9;
        commStats3.total=11;

        QSharedPointer<Commit> inputCommit1 = QSharedPointer<Commit>::create("ausiadnasfaf5as4f5afa",QDateTime::fromString("2021-12-27T16:30:18",Qt::ISODate),"Commit 1","Message 1",Author("Author1","author1@gmail.com"),"www.url1.com",commStats1, QVector<Commit::FileChange>{Commit::FileChange("oldFile1","newFile1",false,true,false)});
        QSharedPointer<Commit> inputCommit2 = QSharedPointer<Commit>::create("d7ay7sda6sd7asd76sa7d",QDateTime::fromString("2021-12-01T10:11:10",Qt::ISODate),"Commit 2","Message 2",Author("Author2","author2@gmail.com"),"www.url2.com",commStats2, QVector<Commit::FileChange>{Commit::FileChange("oldFile2","newFile2",true,true,false)});
        QSharedPointer<Commit> inputCommit3 = QSharedPointer<Commit>::create("a8yhfays8f8saf8sf8f8d",QDateTime::fromString("2021-11-30T09:30:08",Qt::ISODate),"Commit 3","Message 3",Author("Author2","author2@gmail.com"),"www.url3.com",commStats3, QVector<Commit::FileChange>{Commit::FileChange("oldFile3","newFile3",false,false,true)});

        QVector<QSharedPointer<Commit>> commits1;
        commits1.append(inputCommit1);

        QVector<QSharedPointer<Commit>> commits2;
        commits2.append(inputCommit2);
        commits2.append(inputCommit3);


        inputMap->insert(Author("Author1","author1@gmail.com"),commits1);
        inputMap->insert(Author("Author2","author2@gmail.com"),commits2);

        int expectedNumOfCommits = 3;
        int expectedNumOfAuthors = 2;

        ProcessingRepo *pr = ProcessingRepo::getInstance();
        pr->setCommitStatsForRepo(inputRepo,inputMap);
        int returnNumOfCommits = inputRepo->getNumOfCommits();
        int returnNumOfAuthors = inputRepo->getNumOfAuthors();

        CHECK(returnNumOfCommits == expectedNumOfCommits);
        REQUIRE(returnNumOfAuthors == expectedNumOfAuthors);
    }
}

TEST_CASE("ProcessingRepo::parseToRepoProjectNetworkResponse","[function]"){
    SECTION("If ProjectNetworkResponse returns repo, function will create repo with same values"){
        QJsonObject input;
        std::shared_ptr<Network::GitLabProjectResponse> inputNetwork=std::make_shared<Network::GitLabProjectResponse>(input);
        inputNetwork->id=12345678;
        inputNetwork->description ="";
        inputNetwork->name = "Fake group";
        inputNetwork->nameWithNamespace = "projects/2021/fakeGroup";
        inputNetwork->path="group";
        inputNetwork->pathWithNamespace="projects/group";
        inputNetwork->defaultBranch="master";
        inputNetwork->createdAt="2021-12-01T10:11:10";
        inputNetwork->tagList= std::vector<std::string>{};
        inputNetwork->topics= std::vector<std::string>{};
        inputNetwork->sshUrlToRepo="SSH";
        inputNetwork->httpUrlToRepo="HTTP";
        inputNetwork->webUrl="www.url.com";
        inputNetwork->avatarUrl="Avatar url";
        inputNetwork->groupName="";
        inputNetwork->groupId=-1;
        inputNetwork->forksCount=5;
        inputNetwork->starCount=6;
        inputNetwork->lastActivityAt="2021-12-27T16:30:18";

        QSharedPointer<Repo> expected = QSharedPointer<Repo>::create(12345678,"projects/2021/fakeGroup",QDateTime::fromString("2021-12-01T10:11:10",Qt::ISODate).toLocalTime(),"www.url.com","",QDateTime::fromString("2021-12-27T16:30:18",Qt::ISODate).toLocalTime());

        ProcessingRepo *pr = ProcessingRepo::getInstance();
        QSharedPointer<Repo> returnValue = pr->parseToRepoProjectNetworkResponse(inputNetwork);

        REQUIRE(*expected.data() == *returnValue.data());
    }
}


#endif //GITSTAT_REPOPROCESSINGTEST_H
