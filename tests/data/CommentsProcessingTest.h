//
// Created by caca on 27.12.21..
//

#ifndef GITSTAT_COMMENTSPROCESSINGTEST_H
#define GITSTAT_COMMENTSPROCESSINGTEST_H

#include <catch2/catch_test_macros.hpp>
#include <QJsonObject>
#include <QJsonDocument>
#include "../../include/data/ProcessingComments.h"

TEST_CASE("ProcessingComments::removeCommentsForRepoId","[function]"){
    SECTION("If file doesn't have any comments for given repo, there are no changes"){

        QFile comments("fakeComments.json");
        comments.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::Text);
        QTextStream stream(&comments);
        stream << "[\n {\n"
                  "        \"createdAt\": \"2021-12-27T16:30:18\",\n"
                  "        \"message\": \"Comment 1\",\n"
                  "        \"parentId\": \"31055519\",\n"
                  "        \"repoId\": \"31055519\",\n"
                  "        \"type\": \"repo\"\n"
                  "}\n]";
        comments.close();
        int expected = 1;

        ProcessingComments *pc = ProcessingComments::getInstance();
        pc->removeCommentsForRepoId(51236981,"fakeComments.json");
        comments.open(QIODevice::OpenModeFlag::ReadOnly | QIODevice::Text);
        QByteArray jsonComments =  comments.readAll() ;
        comments.close();
        QJsonArray readCommentsArray = QJsonDocument::fromJson(jsonComments).array();
        int size = readCommentsArray.size();

        REQUIRE(expected == size);
    }

    SECTION("If file is empty, there are no changes"){

        QFile comments("fakeComments.json");
        comments.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::Text);
        QTextStream stream(&comments);
        stream << "[\n]";
        comments.close();
        int expected = 0;

        ProcessingComments *pc = ProcessingComments::getInstance();
        pc->removeCommentsForRepoId(51236981,"fakeComments.json");
        comments.open(QIODevice::OpenModeFlag::ReadOnly | QIODevice::Text);
        QByteArray jsonComments =  comments.readAll() ;
        comments.close();
        QJsonArray readCommentsArray = QJsonDocument::fromJson(jsonComments).array();
        int size = readCommentsArray.size();

        REQUIRE(expected == size);
    }

    SECTION("If file has 3 comments, 2 of them for given repo, size of comments will be decreased by 2"){

        int inputRepoId = 31055519;
        QFile comments("fakeComments.json");
        comments.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::Text);
        QTextStream stream(&comments);
        stream << "[\n {\n"
                  "        \"createdAt\": \"2021-12-27T16:30:18\",\n"
                  "        \"message\": \"Comment 1\",\n"
                  "        \"parentId\": \"87654321\",\n"
                  "        \"repoId\": \"87654321\",\n"
                  "        \"type\": \"repo\"\n"
                  "}\n,{\n\n"
                  "         \"createdAt\": \"2021-12-27T16:35:18\",\n\"\n"
                  "         \"message\": \"Comment 2\",\n\"\n"
                  "         \"parentId\": \"31055519\",\n\"\n"
                  "         \"repoId\": \"31055519\",\n\"\n"
                  "         \"type\": \"repo\"\n\"\n"
                  "         \"},"
                  "{\n\n"
             "         \"createdAt\": \"2021-12-27T16:39:18\",\n\"\n"
             "         \"message\": \"Comment 3\",\n\"\n"
             "         \"parentId\": \"31055519\",\n\"\n"
             "         \"repoId\": \"31055519\",\n\"\n"
             "         \"type\": \"repo\"\n\"\n"
             "         \"}"
                  "\n]";
        comments.close();
        ProcessingComments *pc = ProcessingComments::getInstance();
        int expected = 1;

        pc->removeCommentsForRepoId(inputRepoId,"fakeComments.json");
        comments.open(QIODevice::OpenModeFlag::ReadOnly | QIODevice::Text);
        QByteArray jsonComments =  comments.readAll() ;
        comments.close();
        QJsonArray readCommentsArray = QJsonDocument::fromJson(jsonComments).array();
        int size = readCommentsArray.size();

        REQUIRE_FALSE(expected == size);
    }

    SECTION("If file has 3 comments, 2 for given repo (one for author and one for commit in this repo), size of comments will be decreased by 2"){

        int inputRepoId = 31055519;
        QFile comments("fakeComments.json");
        comments.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::Text);
        QTextStream stream(&comments);
        stream << "[\n {\n"
                  "        \"createdAt\": \"2021-12-27T16:30:18\",\n"
                  "        \"message\": \"Comment 1\",\n"
                  "        \"parentId\": \"ahsasd5as67da\",\n"
                  "        \"repoId\": \"31055519\",\n"
                  "        \"type\": \"commit\"\n"
                  "}\n,{\n\n"
                  "         \"createdAt\": \"2021-12-27T16:35:18\",\n\"\n"
                  "         \"message\": \"Comment 2\",\n\"\n"
                  "         \"parentId\": \"author@author.com\",\n\"\n"
                  "         \"repoId\": \"31055519\",\n\"\n"
                  "         \"type\": \"author\"\n\"\n"
                  "         \"},"
                  "{\n\n"
             "         \"createdAt\": \"2021-12-27T16:39:18\",\n\"\n"
             "         \"message\": \"Comment 3\",\n\"\n"
             "         \"parentId\": \"31340769\",\n\"\n"
             "         \"repoId\": \"31340769\",\n\"\n"
             "         \"type\": \"repo\"\n\"\n"
             "         \"}"
                  "\n]";
        comments.close();
        ProcessingComments *pc = ProcessingComments::getInstance();
        int expected = 1;

        pc->removeCommentsForRepoId(inputRepoId,"fakeComments.json");
        comments.open(QIODevice::OpenModeFlag::ReadOnly | QIODevice::Text);
        QByteArray jsonComments =  comments.readAll() ;
        comments.close();
        QJsonArray readCommentsArray = QJsonDocument::fromJson(jsonComments).array();
        int size = readCommentsArray.size();

        REQUIRE_FALSE(expected == size);
    }
}

TEST_CASE("ProcessingComments::parseCommentQJsonToClass","[function]"){
    SECTION("If QJsonObject has all properties, function returns QSharedPointer<Comment> with same values"){
        QJsonObject input {
                              {"createdAt", "2021-12-27T16:30:18"},
                              {"message", "Comment 1"},
                              {"parentId", "87654321"},
                              {"repoId","87654321"},
                              {"type", "repo"}
                          };
        QSharedPointer<Comment> expected = QSharedPointer<Comment>::create(TypeOfComm::repo,"87654321",QDateTime::fromString("2021-12-27T16:30:18",Qt::ISODate),
                                                                           "Comment 1",87654321);

        ProcessingComments *pc = ProcessingComments::getInstance();
        QSharedPointer<Comment> returnValue = pc->parseCommentQJsonToClass(input);

        REQUIRE(*expected.data()==*returnValue.data());
    }
}

TEST_CASE("ProcessingComments::parseCommentClassToQJson","[function]"){
    SECTION("For QSharedPointer<Comment> function returns QJsonObject with same values"){
        QSharedPointer<Comment> input = QSharedPointer<Comment>::create(TypeOfComm::repo,"87654321",QDateTime::fromString("2021-12-27T16:30:18",Qt::ISODate),
                                                                        "Comment 1",87654321);
        QJsonObject expected  {
                                {"createdAt", "2021-12-27T16:30:18"},
                                {"message", "Comment 1"},
                                {"parentId", "87654321"},
                                {"repoId","87654321"},
                                {"type", "repo"}
                              };

        ProcessingComments *pc = ProcessingComments::getInstance();
        QJsonObject returnValue = pc->parseCommentClassToQJson(input);

        REQUIRE(expected==returnValue);
    }
}


TEST_CASE("ProcessingComments::getCommentsForObjectId","[function]"){
    SECTION("If there are no comments for given object, empty vector will be returned"){
        int inputObjectId = 11111111;
        QFile comments("fakeComments.json");
        comments.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::Text);
        QTextStream stream(&comments);
        stream << "[\n {\n"
                  "        \"createdAt\": \"2021-12-27T16:30:18\",\n"
                  "        \"message\": \"Comment 1\",\n"
                  "        \"parentId\": \"asd67asd7asd87\",\n"
                  "        \"repoId\": \"87654321\",\n"
                  "        \"type\": \"commit\"\n"
                  "}\n,{\n\n"
                  "         \"createdAt\": \"2021-12-27T16:35:18\",\n"
                  "         \"message\": \"Comment 2\",\n"
                  "         \"parentId\": \"author@author.com\",\n"
                  "         \"repoId\": \"87654321\",\n"
                  "         \"type\": \"author\"\n"
                  " },"
                  "{\n\n"
             "         \"createdAt\": \"2021-12-27T16:39:18\",\n"
             "         \"message\": \"Comment 3\",\n"
             "         \"parentId\": \"31055519\",\n"
             "         \"repoId\": \"31055519\",\n"
             "         \"type\": \"repo\"\n"
             "   }"
                  "\n]";
        comments.close();
        int expected = 0;

        ProcessingComments *pc = ProcessingComments::getInstance();
        QVector<QSharedPointer<Comment>> returnValue = pc->getCommentsForObjectId(QString::number(inputObjectId),TypeOfComm::repo, inputObjectId,"fakeComments.json");
        int size = returnValue.size();

        REQUIRE(expected == size);
    }

    SECTION("If there is 1 comment for given repo, function will return vector of size 1"){
        int inputObjectId = 87654321;
        TypeOfComm inputType = TypeOfComm::repo;
        QFile comments("fakeComments.json");
        comments.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::Text);
        QTextStream stream(&comments);
        stream << "[\n {\n"
                  "        \"createdAt\": \"2021-12-27T16:30:18\",\n"
                  "        \"message\": \"Comment 1\",\n"
                  "        \"parentId\": \"87654321\",\n"
                  "        \"repoId\": \"87654321\",\n"
                  "        \"type\": \"repo\"\n"
                  "}\n,{\n\n"
                  "         \"createdAt\": \"2021-12-27T16:35:18\",\n"
                  "         \"message\": \"Comment 2\",\n"
                  "         \"parentId\": \"author@author.com\",\n"
                  "         \"repoId\": \"87654321\",\n"
                  "         \"type\": \"author\"\n"
                  " },"
                  "{\n\n"
             "         \"createdAt\": \"2021-12-27T16:39:18\",\n"
             "         \"message\": \"Comment 3\",\n"
             "         \"parentId\": \"31055519\",\n"
             "         \"repoId\": \"31055519\",\n"
             "         \"type\": \"repo\"\n"
             "   }"
                  "\n]";
        comments.close();
        QSharedPointer<Comment> comment = QSharedPointer<Comment>::create(TypeOfComm::repo,"87654321" , QDateTime::fromString("2021-12-27T16:30:18",Qt::ISODate), "Comment 1", 87654321);
        QVector<QSharedPointer<Comment>> expected{comment};
        int expectedSize = 1;

        ProcessingComments *pc = ProcessingComments::getInstance();
        QVector<QSharedPointer<Comment>> returnValue = pc->getCommentsForObjectId(QString::number(inputObjectId), inputType, inputObjectId, "fakeComments.json");
        int returnValueSize = returnValue.size();

        REQUIRE(expectedSize == returnValueSize);
    }

}

TEST_CASE("ProcessingComments::addCommentToRepo","[function]"){
    SECTION("If comment has empty message string, comment won't be added"){

        QSharedPointer<Comment> commentInput = QSharedPointer<Comment>::create(TypeOfComm::repo,"87654321",QDateTime::fromString("2021-12-27T16:30:18",Qt::ISODate),"",87654321);
        QSharedPointer<Repo> repoInput = QSharedPointer<Repo>::create(87654321,"Fake repo",QDateTime::fromString("2021-12-25T16:30:18",Qt::ISODate), "www.url.com","",QDateTime::fromString("2021-12-26T16:30:18",Qt::ISODate));
        QString authorId ="";
        int expectedRepoCommentsSize = 0;

        ProcessingComments *commentProcessor= ProcessingComments::getInstance();
        commentProcessor->addCommentToRepo(repoInput, commentInput,authorId);
        int repoCommentsSize = repoInput->getComments().size();

        REQUIRE(expectedRepoCommentsSize == repoCommentsSize);
    }

    SECTION("If comment has valid value, repo comments size will be increased by 1"){

        QSharedPointer<Comment> commentInput = QSharedPointer<Comment>::create(TypeOfComm::repo,"31055519",QDateTime::fromString("2021-12-27T16:30:18",Qt::ISODate),"fake comment",31055519);
        QSharedPointer<Repo> repoInput = QSharedPointer<Repo>::create(31055519,"Fake repo",QDateTime::fromString("2021-12-25T16:30:18",Qt::ISODate), "www.url.com","",QDateTime::fromString("2021-12-26T16:30:18",Qt::ISODate));
        QString authorId ="";
        int expectedRepoCommentsSize = 1;

        ProcessingComments *commentProcessor= ProcessingComments::getInstance();
        commentProcessor->addCommentToRepo(repoInput, commentInput,authorId);
        int repoCommentsSize = repoInput->getComments().size();

        REQUIRE(expectedRepoCommentsSize == repoCommentsSize);
    }
}

TEST_CASE("ProcessingComments::writeCommentToDisk","[function]"){
    SECTION("If comment has empty message string, comments file won't change"){

        QFile comments("fakeComments.json");
        comments.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::Text);
        QTextStream stream(&comments);
        stream << "[\n {\n"
                  "        \"createdAt\": \"2021-12-27T16:30:18\",\n"
                  "        \"message\": \"Comment 1\",\n"
                  "        \"parentId\": \"31055519\",\n"
                  "        \"repoId\": \"31055519\",\n"
                  "        \"type\": \"repo\"\n"
                  "}\n]";
        comments.close();
        QSharedPointer<Comment> commentInput = QSharedPointer<Comment>::create(TypeOfComm::repo,"87654321",QDateTime::fromString("2021-12-27T16:30:18",Qt::ISODate),"",87654321);
        int expectedFileSize = 1;

        ProcessingComments *commentProcessor= ProcessingComments::getInstance();
        commentProcessor->writeCommentToDisk(commentInput,"fakeComments.json");
        comments.open(QIODevice::OpenModeFlag::ReadOnly | QIODevice::Text);
        QByteArray jsonComments =  comments.readAll() ;
        comments.close();
        QJsonArray commentsArray = QJsonDocument::fromJson(jsonComments).array();
        int fileSize = commentsArray.size();

        REQUIRE(expectedFileSize == fileSize);
    }

    SECTION("If comment has valid value, comments QJsonObject array size will be increased by 1"){

        QFile comments("fakeComments.json");
        comments.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::Text);
        QTextStream stream(&comments);
        stream << "[\n {\n"
                  "        \"createdAt\": \"2021-12-27T16:30:18\",\n"
                  "        \"message\": \"Comment 1\",\n"
                  "        \"parentId\": \"31055519\",\n"
                  "        \"repoId\": \"31055519\",\n"
                  "        \"type\": \"repo\"\n"
                  "}\n]";
        comments.close();
        QSharedPointer<Comment> commentInput = QSharedPointer<Comment>::create(TypeOfComm::repo,"87654321",QDateTime::fromString("2021-12-27T16:30:18",Qt::ISODate),"Comment",87654321);
        int expectedFileSize = 2;

        ProcessingComments *commentProcessor= ProcessingComments::getInstance();
        commentProcessor->writeCommentToDisk(commentInput,"fakeComments.json");
        comments.open(QIODevice::OpenModeFlag::ReadOnly | QIODevice::Text);
        QByteArray jsonComments =  comments.readAll() ;
        comments.close();
        QJsonArray commentsArray = QJsonDocument::fromJson(jsonComments).array();
        int fileSize = commentsArray.size();

        REQUIRE(expectedFileSize == fileSize);
    }
}


#endif //GITSTAT_COMMENTSPROCESSINGTEST_H
