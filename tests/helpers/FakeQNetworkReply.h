//
// Created by luka on 27.12.21..
//

#ifndef GITSTAT_FAKEQNETWORKREPLY_H
#define GITSTAT_FAKEQNETWORKREPLY_H

#include <QNetworkReply>

class FakeQNetworkReply : public QNetworkReply {
    Q_OBJECT

public:
    explicit FakeQNetworkReply(QObject *parent=nullptr);

    void setHttpStatusCode(int code, const QByteArray &statusText = QByteArray());
    void setHeader(QNetworkRequest::KnownHeaders header, const QVariant &value);
    void setRawHeader(const QByteArray& name, const QByteArray& value);
    void setContentType(const QByteArray &contentType);
    void setError(NetworkError errorCode, const QString &errorString);
    void setUrl(const QUrl &url);

    void setContent(const QString &content);

    void abort() override;
    qint64 bytesAvailable() const override;
    bool isSequential() const override;

protected:
    qint64 readData(char *data, qint64 maxSize) override;
};


#endif //GITSTAT_FAKEQNETWORKREPLY_H
