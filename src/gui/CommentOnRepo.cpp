#include "include/gui/CommentOnRepo.h"
#include "ui_CommentOnRepo.h"

CommentOnRepo::CommentOnRepo(QWidget* parent)
  : QDialog(parent)
  , ui(new Ui::CommentOnRepo) {
    ui->setupUi(this);
}

CommentOnRepo::~CommentOnRepo() {
    delete ui;
}

Ui::CommentOnRepo* CommentOnRepo::getUi() {
    return ui;
}
