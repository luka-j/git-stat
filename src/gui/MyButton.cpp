#include "include/gui/MyButton.h"

MyButton::MyButton(const QString& text)
  : QPushButton() {
    this->setText(text);
    connect(this, SIGNAL(clicked()), this, SLOT(reemitClicked()));
}

void MyButton::reemitClicked() {
    auto button = qobject_cast<QPushButton*>(sender());
    emit my_button_clicked(button->text().split(" ").last().toInt());
}
