#include "include/gui/AuthorNode.h"
#include <QPainter>
#include <utility>
#include <utility>

AuthorNode::AuthorNode()
  : QGraphicsItem() {}

AuthorNode::AuthorNode(Author a)
  : QGraphicsItem()
  , author(std::move(std::move(a))) {
    setFlag(ItemIsSelectable);
}

QRectF AuthorNode::boundingRect() const {
    return {0, 0, static_cast<qreal>(Width()), static_cast<qreal>(Height())};
}

void AuthorNode::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) {
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->fillRect(boundingRect(), Qt::white);
    painter->setPen(Qt::black);

    auto tekst = author.getName() + "\n" + author.getEmail();
    painter->drawText(boundingRect(), Qt::AlignHCenter | Qt::AlignVCenter, tekst);
}

qint32 AuthorNode::Height() {
    return NODE_HEIGHT;
}

qint32 AuthorNode::Width() {
    return NODE_WIDTH;
}

Author AuthorNode::getAuthor() {
    return author;
}
