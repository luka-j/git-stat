#include <utility>

#include <utility>

#include "include/gui/SingleRepoData.h"

SingleRepoData::SingleRepoData(QSharedPointer<Repo> repo)
  : repo(std::move(std::move(repo))) {
    qDebug() << "data je kreiran";
}

QVector<Author> SingleRepoData::getAuthors() {
    return repo->getCommitsPerAuthor()->keys();
}

QSharedPointer<Repo> SingleRepoData::getRepo() {
    return repo;
}

QVector<QSharedPointer<Commit>> SingleRepoData::findCommits(const Author& author) {
    return repo->getCommitsPerAuthor()->value(author);
}
