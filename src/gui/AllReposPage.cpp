#include "include/gui/AllReposPage.h"
#include "include/data/DataProcessing.h"
#include "include/gui/MainWindow.h"
#include "include/gui/PopupAddDialog.h"

#include "./ui_MainWindow.h"
#include "./ui_Repo.h"

#include <QMessageBox>
#include <QScrollBar>
#include <string>

AllReposPage::AllReposPage(Ui::MainWindow* ui, MainWindow* w)
  : ui(ui)
  , w(w)
  , dp(DataProcessing::getInstance()) {
    nh = new NetworkResponseHandler(ui);

    connect(dp, &DataProcessing::readyWithCommits, this, &AllReposPage::allRepoInfoReady);
    connect(dp, &DataProcessing::readyProjectInfo, this, &AllReposPage::addNewRepository);
    connect(ui->pbRefreshAll, &QPushButton::clicked, this, &AllReposPage::refreshAllRepositories);
    connect(ui->pbRemoveAll, &QPushButton::clicked, this, &AllReposPage::removeAllRepositories);
}

void AllReposPage::showRepos() {
    rs = dp->onDisk;
    removeAllRepositoriesFromGrid();

    if (rs->empty()) {
        PopupAddDialog pd;
        pd.exec();
    }

    for (int i = 0; i < rs->size(); i++) {

        auto* r = new RepoNode();
        r->setRepo((*rs)[i]);
        r->setNode();
        addRepoToGrid(r);

        if (!(*rs)[i]->getCommitsPerAuthor()->empty()) {
            r->getUi()->pbDetails->setEnabled(true);
            r->getUi()->pbDetails->setToolTip("");
        }
    }
}

void AllReposPage::addNewRepository(const QSharedPointer<Repo>& repo) {

    bool found = false;
    for (auto & i : this->repoi) {
        int id = i->getRepo()->getId();
        if (repo->getId() == id) {
            found = true;
            break;
        }
    }

    if (!found) {

        setMessage("Download in progress...");

        auto* newNode = new RepoNode;
        newNode->setRepo(repo);
        newNode->setWindow(w);
        newNode->setNode();

        addRepoToGrid(newNode);
    } else {
        showRepos();
    }
}

void AllReposPage::addRepoToGrid(RepoNode* r) {
    int size               = this->repoi.size();
    QVector<int> positions = calculateRowAndColumn();
    int row                = positions[0];
    int column             = positions[1];

    ui->glRepos->addWidget(r, row, column, Qt::AlignCenter);

    connect(r, &RepoNode::removeRepository, this, &AllReposPage::removeRepository);
    connect(r, &RepoNode::seeMoreDetails, w, &MainWindow::seeMoreDetails);

    this->repoi.append(r);
    r->setRow(row);
    r->setColumn(column);
    r->show();
}

QVector<int> AllReposPage::calculateRowAndColumn() {

    QVector<int> positions;
    int size   = this->repoi.size();
    int column = size % 4;

    if (size == 0 || column != 0) {
        positions.append(this->row);
        positions.append(column);
        return positions;
    } else {
        positions.append(this->row + 1);
        positions.append(column);
        this->row++;
        return positions;
    }
}

void AllReposPage::allRepoInfoReady(const QSharedPointer<Repo>& repo) {
    int indeks = 0;
    for (int i = 0; i < repoi.size(); i++) {
        if (repoi[i]->getRepo()->getId() == repo->getId()) {
            repoi[i]->setRepo(repo);
            repoi[i]->getUi()->pbDetails->setEnabled(true);
            repoi[i]->getUi()->pbDetails->setToolTip("");
            indeks = i;
            break;
        }
    }

    QString numAuthors = QString::number(repo->getNumOfAuthors());
    QString numCommits = QString::number(repo->getNumOfCommits());
    repoi[indeks]->getUi()->lCommits->setText("Number of commits:");
    repoi[indeks]->getUi()->lAuthors->setText("Number of authors:");
    repoi[indeks]->getUi()->lAuthorsNumber->setText(numAuthors);
    repoi[indeks]->getUi()->lCommitsNumber->setText(numCommits);
}

void AllReposPage::removeRepository(RepoNode* repo) {
    setMessage("");

    int row    = repo->getRow();
    int column = repo->getColumn();
    int id     = repo->getRepo()->getId();

    QLayoutItem* item = ui->glRepos->itemAtPosition(row, column);
    QWidget* widget   = item->widget();
    ui->glRepos->removeItem(item);
    delete widget;

    for (int i = 0; i < this->repoi.size(); i++) {
        if (id == repoi[i]->getRepo()->getId()) {
            this->repoi.removeAt(i);
        }
    }

    dp->removeRepo(id);
    showRepos();
}

void AllReposPage::removeAllRepositories() {
    setMessage("");

    removeAllRepositoriesFromGrid();
    while (!rs->empty()) {
        dp->removeRepo(rs->begin()->data()->getId());
    }

    PopupAddDialog pd;
    pd.exec();
}

void AllReposPage::removeAllRepositoriesFromGrid() {
    for (auto & i : repoi) {

        int r             = i->getRow();
        int c             = i->getColumn();
        QLayoutItem* item = ui->glRepos->itemAtPosition(r, c);
        QWidget* widget   = item->widget();
        ui->glRepos->removeItem(item);
        delete widget;
    }
    this->repoi.clear();
}

void AllReposPage::refreshAllRepositories() {

    for (auto & i : repoi) {

        int id = i->getRepo()->getId();
        dp->getProjectInfo(id, true);
    }

    removeAllRepositoriesFromGrid();
    setMessage("Download in progress...");
}

void AllReposPage::setMessage(const QString& message) {
    ui->tbMessage->setObjectName("message");
    ui->tbMessage->setStyleSheet(
      "#message {color: black; background-color:rgb(203, 228, 235);font: 75 italic 14pt Ubuntu Mono;}");
    ui->tbMessage->setText(message);
}
