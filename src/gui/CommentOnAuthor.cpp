#include "include/gui/CommentOnAuthor.h"
#include "ui_CommentOnAuthor.h"

CommentOnAuthor::CommentOnAuthor(QWidget* parent)
  : QDialog(parent)
  , ui(new Ui::CommentOnAuthor) {
    ui->setupUi(this);
}

CommentOnAuthor::~CommentOnAuthor() {
    delete ui;
}

Ui::CommentOnAuthor* CommentOnAuthor::getUi() {
    return ui;
}
