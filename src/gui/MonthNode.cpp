#include "include/gui/MonthNode.h"
#include <QPainter>
#include <utility>
#include <utility>

MonthNode::MonthNode()
  : QGraphicsItem() {}

MonthNode::MonthNode(QString name)
  : QGraphicsItem()
  , name(std::move(std::move(name))) {}

QRectF MonthNode::boundingRect() const {
    return {0, 0, static_cast<qreal>(Width()), static_cast<qreal>(Height())};
}

void MonthNode::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) {
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->fillRect(boundingRect(), Qt::white);
    painter->setPen(Qt::black);

    const auto text = this->name;
    painter->drawText(boundingRect(), Qt::AlignHCenter | Qt::AlignVCenter, text);
}

QString MonthNode::getName() {
    return this->name;
}

qint32 MonthNode::Height() {
    return 50;
}

qint32 MonthNode::Width() {
    return 90;
}
