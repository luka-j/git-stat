#include "include/gui/CommitNode.h"

CommitNode::CommitNode(QVector<QSharedPointer<Commit>>* commits)
  : QGraphicsItem()
  , commits(commits)
  , brush(QBrush()) {
    setBrush(Qt::green);
}

QRectF CommitNode::boundingRect() const {
    return {0, 0, static_cast<qreal>(Diameter()), static_cast<qreal>(Diameter())};
}

void CommitNode::setBrush(QColor col) {
    this->brush.setStyle(Qt::SolidPattern);
    this->brush.setColor(col);
}

QBrush CommitNode::getBrush() {
    return brush;
}

void CommitNode::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) {

    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setBrush(getBrush());
    painter->drawEllipse(boundingRect());
}

QVector<QSharedPointer<Commit>>* CommitNode::getCommits() {
    return this->commits;
}

qint32 CommitNode::Diameter() {
    return NODE_DIAMETER;
}
