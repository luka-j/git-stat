#include "include/gui/Scene.h"

Scene::Scene(QObject* parent)
  : QGraphicsScene(parent) {}

void Scene::mousePressEvent(QGraphicsSceneMouseEvent* event) {
    QGraphicsItem* item = itemAt(event->scenePos(), QTransform());
    if (item != nullptr) { // da li je uopste u pitanju item
        auto* node_author = dynamic_cast<AuthorNode*>(item);
        if (node_author != nullptr) { // da li je bas autorNode item
            emit author_node_clicked(node_author->getAuthor());
        }
        auto* node_commit = dynamic_cast<CommitNode*>(item);
        if (node_commit != nullptr) {
            emit commit_node_clicked(node_commit->getCommits());
        }
    }
}
