#include "include/gui/MainWindow.h"
#include "./ui_MainWindow.h"
#include "include/gui/CommentOnCommit.h"
#include "include/gui/CommentOnRepo.h"
#include "include/gui/PopupAddDialog.h"
#include "include/gui/PopupTokenDialog.h"

MainWindow::MainWindow(QWidget* parent)
  : QMainWindow(parent)
  , ui(new Ui::MainWindow)
  , scene(new Scene(this)) {
    ui->setupUi(this);
    ui->stackedWidget->setCurrentIndex(0);
}

MainWindow::~MainWindow() {
    delete ui;
}

Ui::MainWindow* MainWindow::getUi() {
    return ui;
}

Scene* MainWindow::getScene() {
    return scene;
}

void MainWindow::on_pbToken_clicked() {
    PopupTokenDialog td;
    td.exec();
}

void MainWindow::on_pbAdd_clicked() {
    PopupAddDialog pd;
    pd.exec();
}

void MainWindow::seeMoreDetails(const QSharedPointer<Repo>& repo) {

    ui->stackedWidget->setCurrentIndex(1);
    QSharedPointer<SingleRepoData> data = QSharedPointer<SingleRepoData>::create(repo);
    auto* check_box             = new CheckBoxList(ui, scene, data);
    auto* plot_timeline         = new PlotTimeline(ui, scene, data, check_box);

    SingleRepoPage* page_two = SingleRepoPage::getInstance();
    page_two->setData(data);
    page_two->setCheckBox(check_box);
    page_two->setPlotTimeline(plot_timeline);

    page_two->connections();
    show_content_of_the_second_page(plot_timeline, check_box, page_two);
}

void MainWindow::show_content_of_the_second_page(PlotTimeline* plot_timeline,
                                                 CheckBoxList* check_box,
                                                 SingleRepoPage* page_two) {
    check_box->create_check_boxes();
    check_box->pb_choose_clicked();
    page_two->create_repo_info_box();
    plot_timeline->getUi()->scrollArea_commitInfo->setVisible(false);
}
