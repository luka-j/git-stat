#include "include/gui/PlotTimeline.h"
#include "ui_MainWindow.h"
#include <QDebug>
#include <utility>
#include <utility>

PlotTimeline::PlotTimeline(Ui::MainWindow* ui,
                           QGraphicsScene* scene,
                           QSharedPointer<SingleRepoData> data,
                           CheckBoxList* cb)
  : ui(ui)
  , scene(scene)
  , data(std::move(std::move(data)))
  , cb(cb) {

    this->local        = QLocale::c();
    this->currentMonth = QDate::currentDate().month();

    qDebug() << "plot_timeline je kreiran";
}

PlotTimeline::~PlotTimeline() {
    qDebug() << "plotitmeline je unisten";
}

void PlotTimeline::plot_vertical_lines(int index, int counter, int numOfAuthors) {
    float diff = ((X_END - X_START) / counter * 1.0);
    QLineF line(QPointF(X_START + diff + diff * index, 50),
                QPointF(X_START + diff + diff * index, Y_START + 100 * numOfAuthors));
    vertical_lines.append(new QGraphicsLineItem(line));

    QPen pen = QPen();
    pen.setWidth(1);
    pen.setStyle(Qt::DashLine);

    vertical_lines[index]->setPen(pen);
    scene->addItem(vertical_lines[index]);
}

void PlotTimeline::calculate_vector_of_months() {
    this->_months.clear();
    int tmp = this->currentMonth;

    for (int i = this->numOfMonths + 1; i > 0; i--) {
        if (tmp <= 0) {
            tmp = 12;
            this->_months.append(new MonthNode(this->local.monthName(tmp, QLocale::FormatType::ShortFormat)));
        } else {
            this->_months.append(new MonthNode(this->local.monthName(tmp, QLocale::FormatType::ShortFormat)));
        }
        tmp -= 1;
    }

    QVector<MonthNode*> result;
    result.reserve(_months.size());
    std::reverse_copy(_months.begin(), _months.end(), std::back_inserter(result));

    this->_months.clear();
    for (auto & i : result) {
        this->_months.append(i);
    }
}

Ui::MainWindow* PlotTimeline::getUi() {
    return this->ui;
}

void PlotTimeline::plot_months_name() {
    float diff = ((X_END - X_START) * 1.0 / (this->numOfMonths * 1.0 + 1));

    calculate_vector_of_months();

    if (!this->_months.empty()) {
        for (int i = 0; i < this->_months.size(); i++) {
            scene->addItem(this->_months[i]);
            this->_months[i]->setPos(X_START + diff * i + diff / 2 - this->_months[i]->Width() / 2, 0);
        }
    }
}

void PlotTimeline::chosen_timeline_for_plot(int index) {

    this->numOfMonths = index;

    delete_vertical_lines();
    delete_months();
    delete_commits();

    if (!this->chosen_authors.empty()) {
        plot_months_name();

        for (int i = 0; i < index; i++) {
            plot_vertical_lines(i, index + 1, this->chosen_authors.size());
        }

        for (int i = 0; i < this->_authors.size(); i++) {

            Author author                           = _authors[i]->getAuthor();
            QVector<QSharedPointer<Commit>> commits = data->findCommits(author);
            create_commits_on_timeline(commits, this->numOfMonths + 1, _authors[i]->Height() / 2 + Y_START + i * 100);
        }
    }

    scene->update(scene->sceneRect());
}

QColor PlotTimeline::calculate_color(int n) {
    switch (n) {
        case 1:
            return {168, 192, 216};
        case 2:
            return {72, 120, 180};
        case 3:
            return {24, 72, 130};
        case 4:
            return {0, 24, 72};
        default:
            return {0, 0, 0};
    }
}

void PlotTimeline::delete_vertical_lines() {
    for (auto & vertical_line : vertical_lines) {

        scene->removeItem(vertical_line);
        delete vertical_line;
    }
    this->vertical_lines.clear();
}

void PlotTimeline::delete_horizontal_lines_and_authors() {
    for (int i = 0; i < this->horizontal_lines.size(); i++) {
        scene->removeItem(_authors[i]);
        delete _authors[i];

        scene->removeItem(horizontal_lines[i]);
        delete horizontal_lines[i];
    }
    _authors.clear();
    horizontal_lines.clear();
}

void PlotTimeline::delete_commits() {
    for (auto & _commit : _commits) {
        for (int j = 0; j < _commit.size(); j++) {
            scene->removeItem(_commit[j]);
            delete _commit[j];
        }
        _commit.clear();
    }
    _commits.clear();
}

void PlotTimeline::delete_months() {
    for (auto & _month : this->_months) {

        scene->removeItem(_month);
        delete _month;
    }
    this->_months.clear();
}

void PlotTimeline::create_commits_on_timeline(QVector<QSharedPointer<Commit>> commits, int months, float y_coord) {
    float big_field   = (X_END - X_START) / months * 1.0;
    float small_field = big_field / 30 * 1.0;
    QVector<CommitNode*> tmp;

    int counter          = -1;
    qint64 previous_diff = -1;
    int same_commit      = 0;

    for (auto & commit : commits) {
        QDate currentDate = QDate::currentDate();
        int tmp_month     = currentDate.month() - months + 1;
        int tmp_year      = currentDate.year();
        if (tmp_month <= 0) {
            tmp_month += 12;
            tmp_year -= 1;
        }
        QDate first_of_month = QDate(tmp_year, tmp_month, 1);
        qint64 diff          = first_of_month.daysTo(commit->getCreatedAt().date());
        if (diff >= 0) {
            if (diff != previous_diff) {
                if (counter != -1) {
                    tmp[counter]->setBrush(calculate_color(tmp[counter]->getCommits()->size()));
                    tmp[counter]->update();
                }
                same_commit++;
                counter++;
                auto* vec = new QVector<QSharedPointer<Commit>>;
                tmp.append(new CommitNode(vec));
                tmp[counter]->getCommits()->append(commit);

                scene->addItem(tmp[counter]);
                float x_coord = X_START + diff * small_field + small_field / 2.0 - tmp[counter]->Diameter() / 2.0;
                tmp[counter]->setPos(x_coord, y_coord - tmp[counter]->Diameter() / 2);

                previous_diff = diff;
            } else {
                tmp[counter]->getCommits()->append(commit);
            }
        }
    }

    if (!tmp.empty()) {
        int tmpSize = tmp.size();
        tmp[tmpSize - 1]->setBrush(calculate_color(tmp[tmpSize - 1]->getCommits()->size()));
        tmp[tmpSize - 1]->update();
    }
    _commits.append(tmp);
}

void PlotTimeline::create_authors_nodes_and_timelines(int i, int counter) {
    QVector<Author> authors = data->getAuthors();

    _authors.append(new AuthorNode(authors[i]));
    int y_coordinate = _authors[counter]->Height() / 2 + Y_START + counter * 100;
    QLineF line(QPointF(X_START, y_coordinate), QPointF(X_END, y_coordinate));
    horizontal_lines.append(new QGraphicsLineItem(line));
    QPen pen = QPen();
    pen.setWidth(2);
    horizontal_lines[counter]->setPen(pen);
    scene->addItem(horizontal_lines[counter]);

    scene->addItem(_authors[counter]);
    _authors[counter]->setPos(0, Y_START + counter * 100);
}

void PlotTimeline::selected_authors(QVector<bool> checked_fields) {
    if (!_authors.empty()) {

        delete_horizontal_lines_and_authors();
        delete_commits();
    }

    int counter = 0;
    for (int i = 0; i < checked_fields.size(); i++) {
        if (checked_fields[i]) {
            create_authors_nodes_and_timelines(i, counter);
            counter++;
        }
    }

    this->chosen_authors.clear();

    for (int i = 0; i < checked_fields.size(); i++) {
        if (checked_fields[i]) {
            this->chosen_authors.append(i);
        }
    }

    emit ui->cb_chose_timeline->activated(this->numOfMonths);

    if (this->chosen_authors.empty()) {

        delete_vertical_lines();
        delete_months();
    }
}
