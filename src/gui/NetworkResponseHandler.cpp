#include "include/gui/NetworkResponseHandler.h"
#include "./ui_MainWindow.h"
#include <QTimer>

NetworkResponseHandler::NetworkResponseHandler(Ui::MainWindow* ui)
  : ui(ui) {
    Network::GitLabNetworking* gl = Network::GitLabNetworking::getInstance();
    qt                            = QSharedPointer<QTimer>::create(this);

    connect(qt.get(), &QTimer::timeout, this, &NetworkResponseHandler::countDown);
    connect(gl, &Network::GitLabNetworking::authCompleted, this, &NetworkResponseHandler::authorization);
    connect(gl, &Network::GitLabNetworking::handleNetworkError, this, &NetworkResponseHandler::downloadError);
    connect(gl, &Network::GitLabNetworking::rateLimited, this, &NetworkResponseHandler::rateLimited);
    connect(gl, &Network::GitLabNetworking::allRequestsFinished, this, &NetworkResponseHandler::allRequestsFinished);
}

void NetworkResponseHandler::setMessage(const QString& message) {
    ui->tbMessage->setObjectName("message");
    ui->tbMessage->setStyleSheet(
      "#message {color: black; background-color:rgb(203, 228, 235);font: 75 italic 14pt Ubuntu Mono;}");
    ui->tbMessage->setText(message);
}

void NetworkResponseHandler::clearMessage() {
    setMessage("");
}

void NetworkResponseHandler::allRequestsFinished() {
    ui->tbMessage->setObjectName("message");
    ui->tbMessage->setStyleSheet(
      "#message {color: green; background-color:rgb(203, 228, 235);font: 75 italic 14pt Ubuntu Mono;}");
    ui->tbMessage->setText("All requests are successfully finished!\n");

    QTimer::singleShot(3000, this, &NetworkResponseHandler::clearMessage);
}

void NetworkResponseHandler::rateLimited(int timeoutSeconds) {
    tmt = timeoutSeconds;

    if (timeoutSeconds == 0) {
        setMessage("Download in progress...");
    } else {
        qt->start(10);
    }
}

void NetworkResponseHandler::countDown() {
    if (this->tmt == 0) {
        qt->stop();
    } else {
        QString message("Made too many network requests.\nTrying again in ");
        message.append(QString::number(this->tmt)).append(" seconds...\n");
        setMessage(message);

        this->tmt--;
        qt->start(1000);
    }
}

void NetworkResponseHandler::authorization(bool valid, const std::string& text) {

    QMessageBox message;

    message.setObjectName("auth");
    message.setStyleSheet("#auth {font-style: italic; background-color: rgb(203, 228, 235)}");
    message.setWindowTitle("Authorization");

    if (valid) {
        QString m("You have successfully signed in as <b>");
        m.append(QString::fromStdString(text)).append("<b>. \n");
        message.setText(m);
    } else {
        message.setText(QString::fromStdString(text).append("\n"));
    }
    message.exec();
}

void NetworkResponseHandler::downloadError(const std::string& error) {
    QMessageBox message;

    message.setObjectName("download");
    message.setStyleSheet("#download {font-style: italic; background-color: rgb(203, 228, 235)}");
    message.setWindowTitle("Download error");

    QString m(QString::fromStdString(error));
    message.setText(m);
    message.exec();
}
