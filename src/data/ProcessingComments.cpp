//
// Created by caca on 25.12.21..
//

#include "include/data/ProcessingComments.h"
#include <filesystem>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QString>

ProcessingComments* ProcessingComments::instance{};

ProcessingComments* ProcessingComments::getInstance() {
    if (ProcessingComments::instance == nullptr) {
        ProcessingComments::instance = new ProcessingComments();
    }
    return ProcessingComments::instance;
}

ProcessingComments::ProcessingComments(QObject* parent)
  : QObject(parent) {
    dataDir = QDir(DIR_PATH);
    if (!dataDir.exists()) {
        if (dataDir.mkpath(".")) {
            qWarning() << "Uspesno kreiran direktorijum\n";
        } else {
            qWarning() << "Nije uspesno kreiran direktorijum\n";
            return;
        }
    }
}

void ProcessingComments::removeCommentsForRepoId(int repoId, const QString& fileName) {
    QFile myFile(fileName);
    if (myFile.open(QIODevice::OpenModeFlag::ReadOnly | QIODevice::Text)) {
        QByteArray val = myFile.readAll();
        myFile.close();

        QJsonArray d = QJsonDocument::fromJson(val).array();
        long long n  = d.size();
        for (int i = n - 1; i >= 0; i--) {
            auto obj = d[i].toObject();
            if (obj["repoId"].toString().toInt() == repoId) {
                d.removeAt(i);
            }
        }
        QJsonDocument toWrite(d);
        if (myFile.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::Truncate)) {
            myFile.write(toWrite.toJson());
            myFile.close();
        } else {
            qWarning() << "Greska pri otvaranju fajla (remove comment)\n";
        }
    }
}

QVector<QSharedPointer<Comment>> ProcessingComments::getCommentsForObjectId(const QString& id,
                                                                            TypeOfComm tip,
                                                                            int repoId,
                                                                            const QString& fileName) {
    QFile myFile(fileName);
    if (!myFile.exists()) {
        qWarning() << "Ne postoji fajl comments.json";
        return QVector<QSharedPointer<Comment>>{};
    }

    QVector<QSharedPointer<Comment>> comments{};
    if (myFile.open(QIODevice::OpenModeFlag::ReadOnly | QIODevice::Text)) {
        QByteArray val = myFile.readAll();
        myFile.close();

        QJsonArray d = QJsonDocument::fromJson(val).array();
        long long n  = d.size();
        for (int i = 0; i < n; i++) {
            auto obj = d[i].toObject();
            if (obj["parentId"].toString() == id && Comment::getTypeOfComm(obj["type"].toString()) == tip &&
                repoId == obj["repoId"].toString().toInt()) {
                comments.push_back(parseCommentQJsonToClass(obj));
            }
        }
    }
    return comments;
}

QSharedPointer<Comment> ProcessingComments::parseCommentQJsonToClass(QJsonObject comment) {
    QString type_str    = comment["type"].toString();
    TypeOfComm type     = Comment::getTypeOfComm(type_str);
    QString parentId    = comment["parentId"].toString();
    int repoId          = comment["repoId"].toString().toInt();
    QDateTime createdAt = QDateTime::fromString(comment["createdAt"].toString(), Qt::ISODate);
    QString message     = comment["message"].toString();

    QSharedPointer<Comment> sharedComment = QSharedPointer<Comment>::create(type, parentId, createdAt, message, repoId);
    return sharedComment;
}

QJsonObject ProcessingComments::parseCommentClassToQJson(const QSharedPointer<Comment>& comment) {
    QJsonObject jsonComment;
    jsonComment.insert("type", comment->typeToString());
    jsonComment.insert("parentId", comment->getParentId());
    jsonComment.insert("createdAt", comment->getCreatedAt().toString(Qt::ISODate));
    jsonComment.insert("message", comment->getMessage());
    jsonComment.insert("repoId", QString::number(comment->getRepoId()));

    return jsonComment;
}

void ProcessingComments::writeCommentToDisk(const QSharedPointer<Comment>& comment, const QString& fileName) {
    if (comment->getMessage() == "") {
        return;
    }
    QFile commentFile(fileName);
    if (!commentFile.exists()) {
        qWarning() << "comments.json ne postoji";
        commentFile.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::Text);
        commentFile.close();
    }
    if (commentFile.open(QIODevice::OpenModeFlag::ReadOnly | QIODevice::Text)) {
        QByteArray readValue = commentFile.readAll();
        commentFile.close();

        QJsonArray comments     = QJsonDocument::fromJson(readValue).array();
        QJsonObject jsonComment = parseCommentClassToQJson(comment);
        comments.push_back(jsonComment);

        if (commentFile.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::Text)) {
            QJsonDocument commentsToWrite(comments);
            commentFile.write(commentsToWrite.toJson());
            commentFile.close();
        }
    }
}

void ProcessingComments::addCommentToRepo(QSharedPointer<Repo> repo, QSharedPointer<Comment> comment, QString& email) {
    if (comment->getMessage() == "") {
        return;
    }
    if (comment->getTypeOfComm() == TypeOfComm::repo) {
        repo->addComment(comment);
    } else {
        QMap<Author, QVector<QSharedPointer<Commit>>>::iterator it;
        QSharedPointer<QMap<Author, QVector<QSharedPointer<Commit>>>> mapp = repo->getCommitsPerAuthor();

        int i = 0;
        for (it = mapp->begin(); it != mapp->end(); ++it) {

            if (comment->getTypeOfComm() == TypeOfComm::author) {
                if (it.key().getEmail() == comment->getParentId()) {
                    auto a                                      = it.key();
                    QVector<QSharedPointer<Commit>> copyCommits = it.value();
                    mapp->remove(a);
                    a.addComment(comment);
                    mapp->insert(a, copyCommits);
                    break;
                }
            } else {
                if (it.key().getEmail() == email) {
                    QVector<QSharedPointer<Commit>>::iterator ip;
                    QVector<QSharedPointer<Commit>> arr = it.value();
                    for (ip = arr.begin(); ip != arr.end(); ip++) {
                        if (ip->data()->getId() == comment->getParentId()) {
                            ip->data()->addComment(comment);
                            break;
                        }
                    }
                }
            }
            i++;
        }
    }
}