#include "../../include/data/Repo.h"

#include <utility>

Repo::Repo(int _id,
           QString _name,
           QDateTime _createdAt,
           QString _webUrl,
           QString _description,
           QDateTime _lastActivity,
           int _numOfCommits,
           int _numOfAuthors,
           QSharedPointer<QMap<Author, QVector<QSharedPointer<Commit>>>> _commitsPerAuthor,
           int _groupId,
           QString _groupName,
           QVector<QSharedPointer<Comment>> comm)
  : id(_id)
  , name(std::move(_name))
  , createdAt(std::move(_createdAt))
  , webUrl(std::move(_webUrl))
  , description(std::move(_description))
  , lastActivity(std::move(_lastActivity))
  , numOfCommits(_numOfCommits)
  , numOfAuthors(_numOfAuthors)
  , commitsPerAuthor(std::move(_commitsPerAuthor))
  , groupId(_groupId)
  , groupName(std::move(_groupName))
  , comments(std::move(comm)) {}

int Repo::getId() const {
    return id;
}

QString Repo::getName() const {
    return name;
}

QDateTime Repo::getCreatedAt() const {
    return createdAt;
}

QSharedPointer<QMap<Author, QVector<QSharedPointer<Commit>>>> Repo::getCommitsPerAuthor() const {
    return commitsPerAuthor;
}

int Repo::getNumOfAuthors() const {
    return numOfAuthors;
}

int Repo::getNumOfCommits() const {
    return numOfCommits;
}

int Repo::getGroupId() const {
    return groupId;
}

QString Repo::getGroupName() const {
    return groupName;
}

QString Repo::getDescription() const {
    return description;
}

QString Repo::getWebUrl() const {
    return webUrl;
}

QDateTime Repo::getLastActivity() const {
    return lastActivity;
}

void Repo::setNumOfCommits(int num) {
    this->numOfCommits = num;
}

void Repo::setNumOfAuthors(int num) {
    this->numOfAuthors = num;
}

void Repo::setCommits(QSharedPointer<QMap<Author, QVector<QSharedPointer<Commit>>>> comm) {
    this->commitsPerAuthor = std::move(comm);
}

QVector<QSharedPointer<Comment>> Repo::getComments() const {
    return comments;
}

void Repo::addComment(const QSharedPointer<Comment>& comm) {
    auto it = std::upper_bound(
      comments.cbegin(), comments.cend(), comm, [](const QSharedPointer<Comment>& a, const QSharedPointer<Comment>& b) {
          return (a->getCreatedAt() < b->getCreatedAt());
      });
    this->comments.insert(it, comm);
}

void Repo::setComments(QVector<QSharedPointer<Comment>> comm) {
    this->comments.swap(comm);
}

bool Repo::operator==(const Repo& rhs) const {
    return id == rhs.id && webUrl == rhs.webUrl && description == rhs.description && lastActivity == rhs.lastActivity &&
           createdAt == rhs.createdAt && name == rhs.name && numOfCommits == rhs.numOfCommits &&
           numOfAuthors == rhs.numOfAuthors && commitsPerAuthor->keys() == rhs.commitsPerAuthor->keys() &&
           commitsPerAuthor->values().size() == rhs.commitsPerAuthor->values().size() && groupId == rhs.groupId &&
           groupName == rhs.groupName && comments == rhs.comments;
}

bool Repo::operator!=(const Repo& rhs) const {
    return !(rhs == *this);
}
