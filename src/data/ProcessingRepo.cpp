//
// Created by caca on 25.12.21..
//

#include "include/data/ProcessingRepo.h"
#include "include/data/ProcessingComments.h"
#include <QJsonDocument>
#include <QJsonObject>

ProcessingRepo* ProcessingRepo::instance{};

ProcessingRepo* ProcessingRepo::getInstance() {
    if (ProcessingRepo::instance == nullptr) {
        ProcessingRepo::instance = new ProcessingRepo();
    }
    return ProcessingRepo::instance;
}

ProcessingRepo::ProcessingRepo(QObject* parent)
  : QObject(parent) {
    commentProcessor = ProcessingComments::getInstance();
    commitProcessor  = ProcessingCommit::getInstance();
}

QSharedPointer<Repo> ProcessingRepo::parseRepoFromQJsonToClass(QJsonObject obj) const {
    int id                = obj["id"].toInt();
    int groupId           = obj["groupId"].toInt();
    QString groupName     = obj["groupName"].toString();
    QString description   = obj["description"].toString();
    QString webUrl        = obj["webUrl"].toString();
    QString lastActivity  = obj["lastActivity"].toString();
    QString createdAt     = obj["createdAt"].toString();
    QString name          = obj["name"].toString();
    int numOfAuthors      = obj["numOfAuthors"].toInt();
    int numOfCommits      = obj["numOfCommits"].toInt();
    QJsonArray mapCommits = obj["commitsPerAuthor"].toArray();

    QSharedPointer<QMap<Author, QVector<QSharedPointer<Commit>>>> commitsPerAuthor =
      ProcessingCommit::createCommitMapFromQJson(mapCommits, id);
    QSharedPointer<Repo> project = QSharedPointer<Repo>::create(id,
                                                                name,
                                                                QDateTime::fromString(createdAt, Qt::ISODate),
                                                                webUrl,
                                                                description,
                                                                QDateTime::fromString(lastActivity, Qt::ISODate),
                                                                numOfCommits,
                                                                numOfAuthors,
                                                                commitsPerAuthor,
                                                                groupId,
                                                                groupName);
    project->setComments(
      ProcessingComments::getCommentsForObjectId(QString::number(project->getId()), TypeOfComm::repo, id));
    return project;
}

QJsonObject ProcessingRepo::parseRepoFromClassToQJson(const QSharedPointer<Repo>& r) {
    QJsonObject repo;
    repo.insert("id", r->getId());
    repo.insert("groupId", r->getGroupId());
    repo.insert("groupName", r->getGroupName());
    repo.insert("description", r->getDescription());
    repo.insert("webUrl", r->getWebUrl());
    repo.insert("lastActivity", r->getLastActivity().QDateTime::toString(Qt::ISODate));
    repo.insert("createdAt", r->getCreatedAt().QDateTime::toString(Qt::ISODate));
    repo.insert("name", r->getName());
    repo.insert("numOfAuthors", r->getNumOfAuthors());
    repo.insert("numOfCommits", r->getNumOfCommits());

    QJsonArray commitsPerAuth;
    commitsPerAuth = ProcessingCommit::createQJsonFromCommitMap(r->getCommitsPerAuthor());
    repo.insert("commitsPerAuthor", commitsPerAuth);
    return repo;
}

QSharedPointer<QVector<QSharedPointer<Repo>>> ProcessingRepo::readAllReposFromDisk(const QString& path) const {
    QSharedPointer<QVector<QSharedPointer<Repo>>> repos = QSharedPointer<QVector<QSharedPointer<Repo>>>::create();

    QFile myFile(path);
    if (!myFile.exists()) {
        qWarning() << "repos.json ne postoji";
        myFile.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::Text);
        myFile.close();
    }
    if (myFile.open(QIODevice::OpenModeFlag::ReadOnly | QIODevice::Text)) {
        QByteArray val = myFile.readAll();
        myFile.close();

        QJsonArray d = QJsonDocument::fromJson(val).array();
        foreach (const auto& pom, d) {
            QJsonObject obj        = pom.toObject();
            QSharedPointer<Repo> r = parseRepoFromQJsonToClass(obj);
            repos->push_back(r);
        }
    } else {
        QString errMsg;
        QFileDevice::FileError err = QFileDevice::NoError;
        errMsg                     = myFile.errorString();
        err                        = myFile.error();
        qWarning() << "Ne radi otvaranje fajla\n" << errMsg << "\n" << err;
    }
    return repos;
}

void ProcessingRepo::writeAllReposToDisk(const QSharedPointer<QVector<QSharedPointer<Repo>>>& repos,
                                         const QString& path) {
    QFile myFile(path);
    if (!myFile.exists()) {
        qWarning() << "Ne postoji fajl (Repo-writeToDisk)\n";
        myFile.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::Text); // todo check if okey
        myFile.close();
    }

    if (myFile.open(QIODevice::OpenModeFlag::WriteOnly | QIODevice::Text)) {
        QJsonArray arr;
        for (const auto& elem : *repos) {
            QJsonObject pom = parseRepoFromClassToQJson(elem);
            arr.push_back(pom);
        }
        QJsonDocument toWrite(arr);
        myFile.write(toWrite.toJson());
        myFile.close();
    } else {
        QString errMsg;
        QFileDevice::FileError err = QFileDevice::NoError;
        errMsg                     = myFile.errorString();
        err                        = myFile.error();
        qWarning() << "Ne radi otvaranje fajla\n" << errMsg << "\n" << err;
    }
}

QSharedPointer<Repo> ProcessingRepo::parseToRepoProjectNetworkResponse(
  const std::shared_ptr<Network::GitLabProjectResponse>& projectInfo) {
    int& id                 = projectInfo->id;
    int groupId             = projectInfo->groupId;
    std::string groupName   = projectInfo->groupName;
    std::string webUrl      = projectInfo->webUrl;
    QString lastActivity    = QString::fromStdString(projectInfo->lastActivityAt);
    QString createdAt       = QString::fromStdString(projectInfo->createdAt);
    std::string description = projectInfo->description;
    std::string name        = projectInfo->nameWithNamespace;
    QSharedPointer<QMap<Author, QVector<QSharedPointer<Commit>>>> sorted =
      QSharedPointer<QMap<Author, QVector<QSharedPointer<Commit>>>>::create();
    auto repo = QSharedPointer<Repo>::create(id,
                                             QString::fromStdString(name),
                                             QDateTime::fromString(createdAt, Qt::ISODate).toLocalTime(),
                                             QString::fromStdString(webUrl),
                                             QString::fromStdString(description),
                                             QDateTime::fromString(lastActivity, Qt::ISODate).toLocalTime(),
                                             0,
                                             0,
                                             sorted,
                                             groupId,
                                             QString::fromStdString(groupName));
    return repo;
}

void ProcessingRepo::setCommitStatsForRepo(
  QSharedPointer<Repo> repo,
  const QSharedPointer<QMap<Author, QVector<QSharedPointer<Commit>>>>& commitsMap) {
    int numOfAuthors = commitsMap->keys().size();
    int numOfCommits = 0;

    QMap<Author, QVector<QSharedPointer<Commit>>>::iterator it;
    for (it = commitsMap->begin(); it != commitsMap->end(); it++) {
        numOfCommits += it.value().size();
    }

    repo->setNumOfCommits(numOfCommits);
    repo->setNumOfAuthors(numOfAuthors);
    repo->setCommits(commitsMap);
}
