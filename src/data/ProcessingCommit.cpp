//
// Created by caca on 25.12.21..
//

#include "include/data/ProcessingCommit.h"

ProcessingCommit* ProcessingCommit::instance{};

ProcessingCommit* ProcessingCommit::getInstance() {
    if (ProcessingCommit::instance == nullptr) {
        ProcessingCommit::instance = new ProcessingCommit();
    }
    return ProcessingCommit::instance;
}

ProcessingCommit::ProcessingCommit(QObject* parent)
  : QObject(parent) {}

QSharedPointer<Commit> ProcessingCommit::commitFromQJsonToClass(const QJsonObject& obj) {
    QString id         = obj["id"].toString();
    QJsonObject author = obj["author"].toObject();
    QString name       = author["name"].toString();
    QString email      = author["email"].toString();
    Author a           = Author(name, email);

    QDateTime createdAt    = QDateTime::fromString(obj["createdAt"].toString(), Qt::ISODate);
    QString title          = obj["title"].toString();
    QString message        = obj["message"].toString();
    QString webUrl         = obj["webUrl"].toString();
    QJsonObject stats      = obj["stats"].toObject();
    int add                = stats["additions"].toInt();
    int del                = stats["deletions"].toInt();
    int tot                = stats["total"].toInt();
    Commit::CommitStats cs = Commit::CommitStats();
    cs.additions           = add;
    cs.deletions           = del;
    cs.total               = tot;

    QJsonArray changesArray = obj["changes"].toArray();
    QVector<Commit::FileChange> niz;
    for (auto elem : changesArray) {
        QJsonObject pom = elem.toObject();
        QString oldFile = pom["oldFilename"].toString();
        QString newFile = pom["newFilename"].toString();
        bool creat      = pom["isFileCreated"].toBool();
        bool rename     = pom["isFileRenamed"].toBool();
        bool deleted    = pom["isFileDeleted"].toBool();

        Commit::FileChange fc = Commit::FileChange(oldFile, newFile, creat, rename, deleted);
        niz.push_back(fc);
    }
    QSharedPointer<Commit> c = QSharedPointer<Commit>::create(id, createdAt, title, message, a, webUrl, cs, niz);
    return c;
}

QJsonObject ProcessingCommit::commitFromClassToQJson(const QSharedPointer<Commit>& commits) {
    QJsonObject comm;
    comm.insert("id", commits->getId());
    QJsonObject author;
    author.insert("name", commits->getAuthor().getName());
    author.insert("email", commits->getAuthor().getEmail());
    comm.insert("author", author);
    comm.insert("message", commits->getMessage());
    comm.insert("createdAt", commits->getCreatedAt().QDateTime::toString(Qt::ISODate));
    comm.insert("title", commits->getTitle());
    comm.insert("webUrl", commits->getWebUrl());

    QJsonObject stats;
    stats.insert("additions", commits->getStats().additions);
    stats.insert("deletions", commits->getStats().deletions);
    stats.insert("total", commits->getStats().total);
    comm.insert("stats", stats);

    QJsonArray changesArray;
    for (const auto& elem : commits->getFileChanges()) {
        QJsonObject changeElem;
        changeElem.insert("oldFilename", elem.oldFilename);
        changeElem.insert("newFilename", elem.newFilename);
        changeElem.insert("isFileCreated", elem.isFileCreated);
        changeElem.insert("isFileRenamed", elem.isFileRenamed);
        changeElem.insert("isFileDeleted", elem.isFileDeleted);

        changesArray.push_back(changeElem);
    }
    comm.insert("changes", changesArray);
    return comm;
}

QJsonArray ProcessingCommit::createQJsonFromCommitMap(
  const QSharedPointer<QMap<Author, QVector<QSharedPointer<Commit>>>>& commitMap) {
    QJsonArray createdMap;

    QMap<Author, QVector<QSharedPointer<Commit>>>::iterator i;
    for (i = commitMap->begin(); i != commitMap->end(); ++i) {
        const Author& auth = i.key();
        QJsonValue aName   = auth.getName();
        QJsonValue aEmail  = auth.getEmail();
        QJsonObject author;
        author.insert("name", aName);
        author.insert("email", aEmail);

        QJsonArray commits;
        for (const auto& com : i.value()) {
            QJsonObject pom = ProcessingCommit::commitFromClassToQJson(com);
            commits.push_back(pom);
        }
        QJsonObject el;
        el.insert("author", author);
        el.insert("commits", commits);
        createdMap.push_back(el);
    }
    return createdMap;
}

QSharedPointer<QMap<Author, QVector<QSharedPointer<Commit>>>> ProcessingCommit::createCommitMapFromQJson(
  const QJsonArray& commitMap,
  int repoId) {
    QSharedPointer<QMap<Author, QVector<QSharedPointer<Commit>>>> commitsPerAuthor =
      QSharedPointer<QMap<Author, QVector<QSharedPointer<Commit>>>>::create();
    if (commitMap.empty()) {
        return commitsPerAuthor;
    }
    for (auto pom : commitMap) {
        QJsonObject element = pom.toObject();
        QString autName     = ((element["author"].toObject())["name"]).toString();
        QString autEmail    = ((element["author"].toObject())["email"]).toString();
        Author a =
          Author(autName, autEmail, ProcessingComments::getCommentsForObjectId(autEmail, TypeOfComm::author, repoId));
        QJsonArray commitsValueArray = element["commits"].toArray();

        QVector<QSharedPointer<Commit>> commits;
        for (const auto commitValue : commitsValueArray) {
            QSharedPointer<Commit> comm = ProcessingCommit::commitFromQJsonToClass(commitValue.toObject());
            comm->setComment(ProcessingComments::getCommentsForObjectId(comm->getId(), TypeOfComm::commit, repoId));
            commits.push_back(comm);
        }

        commitsPerAuthor->insert(a, commits);
    }
    return commitsPerAuthor;
}

QSharedPointer<Commit> ProcessingCommit::parseToCommitCommitNetworkResponse(
  int projectId,
  const std::shared_ptr<Network::GitLabCommitResponse>& commitInfo) {
    std::string id                                   = commitInfo->id;
    std::string authorName                           = commitInfo->authorName;
    std::string authorEMail                          = commitInfo->authorEmail;
    QString createdAt                                = QString::fromStdString(commitInfo->createdAt);
    std::string title                                = commitInfo->title;
    std::string message                              = commitInfo->message;
    std::string webUrl                               = commitInfo->webUrl;
    Network::GitLabCommitResponse::CommitStats stats = commitInfo->stats;
    QVector<Commit::FileChange> changes{};
    for (auto&& el : commitInfo->fileChanges)
        changes.emplace_back(*el);

    // TODO it can be better
    Commit::CommitStats cs{};
    cs.additions = stats.additions;
    cs.deletions = stats.deletions;
    cs.total     = stats.total;

    QString email = QString::fromStdString(authorEMail);
    Author a      = Author(QString::fromStdString(authorName), email);
    a.setComments(ProcessingComments::getCommentsForObjectId(email, TypeOfComm::author, projectId));
    auto c = QSharedPointer<Commit>::create(QString::fromStdString(id),
                                            QDateTime::fromString(createdAt, Qt::ISODate).toLocalTime(),
                                            QString::fromStdString(title),
                                            QString::fromStdString(message),
                                            a,
                                            QString::fromStdString(webUrl),
                                            cs,
                                            changes);
    c->setComment(
      ProcessingComments::getCommentsForObjectId(QString::fromStdString(id), TypeOfComm::commit, projectId));

    return c;
}
