//
// Created by luka on 26.12.21..
//

#include "include/network/NetworkResponseParser.h"
#include "include/network/NetworkUtils.h"
#include "include/network/GitLabNetworking.h"
#include <QJsonParseError>
#include <QThread>
#include <utility>

using namespace Network;

void NetworkResponseParser::parseNetworkError(QNetworkReply* reply) {
    int requestId = reply->property(PROP_REQUEST_ID).toInt();
    if (!reply->error()) {
        qWarning() << "parseNetworkError called on non-error reply, ignoring. requestId:" << requestId;
        return;
    }
    qWarning() << "NETWORK_ERROR:" << reply->errorString();
    auto errorMsg   = mapErrorMessage(reply);
    auto statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
    if (isStatusRateLimited(statusCode)) {
        parseRateLimitError(reply);
        return;
    } else if (!statusCode.isNull()) {
        int httpStatus = statusCode.toInt();
        errorMsg += " Status: " + std::to_string(httpStatus);
    }

    auto uniqueId = makeUniqueId(reply);
    if(statusCode.toInt() == 401 &&
        networkContext->authToken != reply->request().rawHeader(QByteArray::fromStdString(TOKEN_HEADER_NAME)).toStdString() &&
        !networkContext->retriedRequests.contains(uniqueId)) {
        // we've already reset the token, just retry the request
        networkContext->retriedRequests.insert(uniqueId); //precaution
        retryAfter(reply, 0, false);
    }
    if(statusCode.toInt() == 401 && !networkContext->authToken.empty() && !networkContext->retriedRequests.contains(uniqueId)) {
        networkContext->retriedRequests.insert(uniqueId); //prevent infinite retry, just in case
        networkContext->authToken = "";
        saveAuthToken("");
        retryAfter(reply, 0, false);
        emit authTokenErased("Token za autentifikaciju više nije validan. Svi budući zahtevi će biti napravljeni bez njega.");
    } else if (!shouldErrorBeRetried(reply) || networkContext->retriedRequests.contains(uniqueId)) {
        emit fireError(errorMsg, requestId);
    } else {
        networkContext->retriedRequests.insert(uniqueId);
        retryAfter(reply, errorCoolOffTime, false);
    }

    reply->deleteLater();
}

void NetworkResponseParser::parseResponseSlot() {
    auto* reply = dynamic_cast<QNetworkReply*>(sender());
    parseResponse(reply);
}

void NetworkResponseParser::parseRateLimitError(QNetworkReply* reply) {
    auto timeNow          = getUnixTimestamp();
    int retryAfterSeconds = hardRateLimitCoolOffTime;
    if (reply->hasRawHeader("Retry-After")) {
        retryAfterSeconds = reply->rawHeader("Retry-After").toInt() + 1;
    } else {
        qWarning() << "We've got 429, but no Retry-After header is present. Defaulting to 60.";
    }

    qWarning() << "RATELIMITED: Sleeping for" << retryAfterSeconds;
    emit rateLimited(retryAfterSeconds);
    retryAfter(reply, retryAfterSeconds, timeNow < networkContext->rateLimitResetsAt);
    emit rateLimited(0);
}

void NetworkResponseParser::retryAfter(QNetworkReply* reply, int seconds, bool forceSleep) {
    if (reply == nullptr) {
        qCritical() << "Asked to retry null reply!";
        fireError("An unexpected error has occurred. Please try again.", 0);
        return;
    }
    if (networkRequester == nullptr) {
        qCritical() << "Asked to retry reply, but no networkRequester is assigned!";
        fireError("An unexpected error has occurred. Please try again.", 0);
        return;
    }

    auto responseHandler = reply->property(PROP_HANDLER);
    if (responseHandler.isNull()) {
        qCritical() << "CRITICAL: network request doesn't contain a handler, making retry impossible.";
        return;
    }

    auto timeNow          = getUnixTimestamp();
    qlonglong requestTime = reply->property(PROP_TIME_CREATED).toLongLong();
    bool haveSleptSoon    = timeNow - networkContext->lastSleepTime <= minSecondsBetweenSleeps;
    bool isRequestStale   = timeNow - requestTime > staleRequestThreshold;
    if (forceSleep || (!isRequestStale && !haveSleptSoon)) {
        qWarning() << "Asked to retry request. Sleeping for" << seconds << " (force:" << forceSleep << ")...";
        QThread::sleep(seconds);
        networkContext->lastSleepTime = getUnixTimestamp();
    } else {
        qWarning() << "Asked to retry request, but we've already slept recently";
    }

    auto requestId = reply->property(PROP_REQUEST_ID).toInt();
    auto commitId  = reply->property(PROP_COMMIT_ID).toString().toStdString();
    networkRequester->doGetRequest(reply->request().url().toString().toStdString(),
                                   responseHandler.value<ResponseHandlerFunc>(),
                                   requestId,
                                   commitId);
    reply->deleteLater();
}

NetworkResponseParser::NetworkResponseParser(QObject* parent,
                                             std::shared_ptr<NetworkContext> networkContext,
                                             int minSecondsBetweenSleeps,
                                             int staleRequestThreshold,
                                             int errorCoolOffTime,
                                             int hardRateLimitCoolOffTime)
  : QObject(parent)
  , networkContext(std::move(networkContext))
  , minSecondsBetweenSleeps(minSecondsBetweenSleeps)
  , staleRequestThreshold(staleRequestThreshold)
  , errorCoolOffTime(errorCoolOffTime)
  , hardRateLimitCoolOffTime(hardRateLimitCoolOffTime) {}

void NetworkResponseParser::setNetworkRequester(NetworkRequester* networkRequester) {
    this->networkRequester = networkRequester;
}

void NetworkResponseParser::parseResponse(QNetworkReply* reply) {
    if (reply == nullptr) {
        qCritical() << "Reply is nullptr: probably casting failed.";
        fireError("An unknown error occurred while downloading the project.", -1);
        return;
    }

    auto requestId           = reply->property(PROP_REQUEST_ID).toInt();
    auto responseHandlerProp = reply->property(PROP_HANDLER);
    if (responseHandlerProp.isNull()) {
        qCritical() << "CRITICAL: network request doesn't contain a handler.";
        emit fireError("An unexpected error has occurred while downloading data. Please try again.", requestId);
        return;
    }

    networkContext->populateRateLimitDataFromReply(reply);
    if (reply->error()) {
        parseNetworkError(reply);
        return;
    }

    auto response = reply->readAll();
    QJsonParseError err;
    auto parsedResponse = QJsonDocument::fromJson(response, &err);
    if (parsedResponse.isNull()) {
        qCritical() << "Error parsing GitLab response:" << err.errorString();
        fireError("An unknown error occurred while downloading project from GitLab. Try again later.", requestId);
        reply->deleteLater();
        return;
    }

    emit handleResponse(responseHandlerProp.value<ResponseHandlerFunc>(), reply, parsedResponse);

    reply->deleteLater();
}
