//
// Created by luka on 9.12.21..
//

#include "include/network/GitLabCommitResponse.h"

#define JSON2STR(key) json[key].toString().toStdString()
Network::GitLabCommitResponse::GitLabCommitResponse(QJsonObject& json) {
    id        = JSON2STR("id");
    shortId   = JSON2STR("short_id");
    createdAt = JSON2STR("created_at");
    for (auto&& it : json["parent_ids"].toArray())
        parentIds.push_back(it.toString().toStdString());
    title          = JSON2STR("title");
    message        = JSON2STR("message");
    authorName     = JSON2STR("author_name");
    authorEmail    = JSON2STR("author_email");
    authoredDate   = JSON2STR("authored_date");
    committerName  = JSON2STR("committer_name");
    committerEmail = JSON2STR("committer_email");
    committedDate  = JSON2STR("committed_date");
    webUrl         = JSON2STR("web_url");
    auto statsJson = json["stats"].toObject();
    stats = CommitStats{statsJson["additions"].toInt(), statsJson["deletions"].toInt(), statsJson["total"].toInt()};
}
#undef JSON2STR

Network::GitLabCommitResponse::operator QString() const {
    QString str(("GitLabCommitResponse{id: " + id + ", shortId: " + shortId + ", createdAt: " + createdAt +
                 ", parentIds_len: " + std::to_string(parentIds.size()) + ", title: " + title +
                 ", message: " + message + ", authorName: " + authorName + ", authorEmail: " + authorEmail +
                 ", authoredDate: " + authoredDate + ", committerName: " + committerName +
                 ", committerEmail: " + committerEmail + ", committedDate: " + committedDate + ", webUrl: " + webUrl +
                 ", stats: " + (std::string)stats + " fileChanges: [")
                  .c_str());
    for (auto&& it : fileChanges) {
        str.append(*it);
        str.append(", ");
    }
    str.append("]}");
    return str;
}

Network::GitLabCommitResponse::FileChange::FileChange(const QJsonObject& json) {
    oldFilename   = json["old_path"].toString().toStdString();
    newFilename   = json["new_path"].toString().toStdString();
    isFileCreated = json["new_file"].toBool();
    isFileRenamed = json["renamed_file"].toBool();
    isFileDeleted = json["deleted_file"].toBool();
}

Network::GitLabCommitResponse::FileChange::operator QString() const {
    QString str(((std::string) * this).c_str());
    return str;
}

Network::GitLabCommitResponse::FileChange::operator std::string() const {
    return "FileChange{oldFilename: " + oldFilename + " newFilename: " + newFilename +
           " isFileCreated: " + std::to_string(isFileCreated) + " isFileRenamed: " + std::to_string(isFileRenamed) +
           " isFileDeleted: " + std::to_string(isFileDeleted) + "}";
}

Network::GitLabCommitResponse::CommitStats::operator QString() const {
    QString str(((std::string) * this).c_str());
    return str;
}

Network::GitLabCommitResponse::CommitStats::operator std::string() const {
    return "CommitStats{additions: " + std::to_string(additions) + " deletions: " + std::to_string(deletions) +
           " total: " + std::to_string(total) + "}";
}
