//
// Created by luka on 26.12.21..
//

#include "include/network/GroupContext.h"

using namespace Network;

bool GroupContext::erase(int groupId) {
    return contextMap.erase(groupId);
}

bool GroupContext::softErase(int groupId) {
    if (!contextMap.contains(groupId))
        return false;
    contextMap[groupId]->maybeDangling = true;
    return true;
}

std::shared_ptr<GroupContext::GroupContextData> GroupContext::createOrGet(int groupId) {
    insertMutex.lock();
    if (contextMap.contains(groupId)) {
        if (contextMap[groupId]->maybeDangling) {
            contextMap.erase(groupId);
        } else {
            insertMutex.unlock();
            return contextMap[groupId];
        }
    }
    contextMap[groupId] = std::make_shared<GroupContextData>();
    insertMutex.unlock();
    return nullptr;
}

unsigned long GroupContext::size() {
    return contextMap.size();
}

bool GroupContext::setData(const std::shared_ptr<Network::GitLabGroupResponse>& data) {
    if (!contextMap.contains(data->id))
        return false;
    contextMap[data->id]->groupInfo = data;
    return true;
}

bool GroupContext::insertProject(int groupId, const std::shared_ptr<GitLabProjectResponse>& project) {
    if (!contextMap.contains(groupId))
        return false;
    contextMap[groupId]->groupInfo->projects.push_back(project);
    return true;
}

std::shared_ptr<GitLabGroupResponse> GroupContext::get(int groupId) {
    if (!contextMap.contains(groupId))
        return nullptr;
    return contextMap[groupId]->groupInfo;
}
