//
// Created by luka on 26.12.21..
//

#include "include/network/ProjectContext.h"

#include <utility>

using namespace Network;

bool ProjectContext::incrementCommitDownloaded(int projectId) {
    if (!contextMap.contains(projectId))
        return false;
    contextMap[projectId]->downloadedCommits++;
    return true;
}

bool ProjectContext::areAllCommitsDownloaded(int projectId) {
    if (!contextMap.contains(projectId))
        return false;
    auto ctx = contextMap[projectId];
    return ctx->finishedCommitCount && ctx->downloadedCommits == ctx->totalCommits;
}

bool ProjectContext::erase(int projectId) {
    return contextMap.erase(projectId);
}

bool ProjectContext::softErase(int id) {
    if (!contextMap.contains(id))
        return false;
    contextMap[id]->maybeDangling = true;
    return true;
}

std::shared_ptr<ProjectContext::ProjectContextData> ProjectContext::createOrGet(int id) {
    insertMutex.lock();
    if (contextMap.contains(id)) {
        if (contextMap[id]->maybeDangling) {
            contextMap.erase(id);
        } else {
            insertMutex.unlock();
            return contextMap[id];
        }
    }
    contextMap[id]            = std::make_shared<ProjectContextData>();
    contextMap[id]->projectId = id;
    insertMutex.unlock();
    return nullptr;
}

unsigned long ProjectContext::size() {
    return contextMap.size();
}

bool ProjectContext::isEmpty() {
    for (auto& it : contextMap) {
        if (!it.second->maybeDangling) {
            return false;
        }
    }
    return true;
}

std::shared_ptr<ProjectContext::ProjectContextData> ProjectContext::get(int id) {
    if (!contextMap.contains(id))
        return nullptr;
    return contextMap[id];
}

std::shared_ptr<std::list<std::shared_ptr<GitLabCommitResponse>>> ProjectContext::getCommits(int projectId) {
    if (!contextMap.contains(projectId))
        return nullptr;
    return contextMap[projectId]->commits.getValues();
}

std::shared_ptr<GitLabCommitResponse> ProjectContext::getCommit(int projectId, std::string commitId) {
    if (!contextMap.contains(projectId))
        return nullptr;
    return contextMap[projectId]->commits.get(std::move(commitId));
}

bool ProjectContext::addCommitCount(int projectId, int commitCount) {
    if (!contextMap.contains(projectId))
        return false;
    auto ctx = contextMap[projectId];
    if (ctx->finishedCommitCount)
        return false;
    ctx->totalCommits += commitCount;
    return true;
}

bool ProjectContext::finishCommitCount(int projectId) {
    if (!contextMap.contains(projectId))
        return false;
    contextMap[projectId]->finishedCommitCount = true;
    return true;
}

bool ProjectContext::exists(int projectId) {
    return contextMap.contains(projectId);
}

bool ProjectContext::insertCommits(int projectId, const std::shared_ptr<GitLabCommitResponse>& commit) {
    if (!contextMap.contains(projectId))
        return false;
    contextMap[projectId]->commits.insert(commit->id, commit);
    return true;
}

long long ProjectContext::getTotalCommitsCount(int projectId) {
    return contextMap[projectId]->totalCommits;
}

bool ProjectContext::isFinishedCommitCount(int projectId) {
    return contextMap[projectId]->finishedCommitCount;
}

long long ProjectContext::getDownloadedCommitsCount(int projectId) {
    return contextMap[projectId]->downloadedCommits;
}
